char* stringToLong(long x) {
  char temp[14];

  return temp;
}

void displayOn(int state)
{

  String strTemp = "";
  char temp[20];// may be need more test ndk
  char DateTime[13] = {0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20};
  micTemp = micCount;
  uint8_t SS = sendingDataSet.SIGNAL_STRENGTH;

  switch (state) {


    case 0:


      nodes(1, WHITE);
      nodes(2, WHITE);
      nodes(3, WHITE);
      myTime.substring(0, myTime.indexOf(" ", myTime.indexOf("/"))).toCharArray(DateTime, 13);
#ifdef __GSM__
      for (byte ccc = 0; ccc < 13; ccc++) if (DateTime[ccc] == 0x00)DateTime[ccc] = 0x20;
      if ((SS >= 1) && (SS < 16))DateTime[11] = 0x7c;
      else if (SS >= 16) DateTime[11] = 0x7d;
      if ((SS >= 4) && (SS < 20)) DateTime[12] = 0x7c;
      else if (SS >= 20) DateTime[12] = 0x7d;
#endif
      lcdPrint(DateTime, 1, BLACK);
      for (byte ccc = 0; ccc < 13; ccc++) DateTime[ccc] = 0x20;
      myTime.substring(myTime.indexOf(" ", myTime.indexOf("/") + 1) + 1, 19).toCharArray(DateTime, 13);
#ifdef __GSM__
      for (byte ccc = 0; ccc < 13; ccc++) if (DateTime[ccc] == 0x00)DateTime[ccc] = 0x20;
      if ((SS >= 8) && (SS < 24))DateTime[11] = 0x7c;
      else if (SS >= 24) DateTime[11] = 0x7d;
      if ((SS >= 12) && (SS < 28)) DateTime[12] = 0x7c;
      else if (SS >= 28) DateTime[12] = 0x7d;
#endif
      lcdPrint(DateTime, 2, BLACK);
      strTemp = (String)(sendingDataSet.PARAMETERS[12] + sendingDataSet.PARAMETERS[13] + sendingDataSet.PARAMETERS[14]);
      strTemp.toCharArray(temp, 20);
      strcat(temp, "kWh");
      lcdPrint(temp, 3, BLACK);
      dtostrf((aActivePower + bActivePower + cActivePower), 1, 1, temp);
      strcat(temp, "kW");
      lcdPrint(temp, 4, BLACK);
      dtostrf(((powerPara[3] + powerPara[4] + powerPara[5]) / 3), 1, 1, temp);
      strcat(temp, "V");
      lcdPrint(temp, 5, BLACK);
      dtostrf((powerPara[2] + powerPara[1] + powerPara[0]), 1, 1, temp);
      strcat(temp, "A");
#ifdef __GSM__
      if ((!CONNECTION_OPEN) || (!sendDataFlag))lcdPrint(temp, 6, BLACK);
#else
      lcdPrint(temp, 6, BLACK);
#endif


      break;
    case 1:

      nodes(2, WHITE);
      nodes(3, WHITE);
      nodes(1, BLACK);
      strTemp = (String)sendingDataSet.PARAMETERS[12];
      strTemp.toCharArray(temp, 20);
      strcat(temp, "kWh");
      lcdPrint(temp, 1, BLACK);
      dtostrf(aActivePower, 1, 1, temp);
      strcat(temp, "kW");
      lcdPrint(temp, 2, BLACK);
      dtostrf(powerPara[3], 1, 1, temp);
      strcat(temp, "V");
      lcdPrint(temp, 3, BLACK);
      dtostrf(powerPara[0], 1, 1, temp);
      strcat(temp, "A");
      lcdPrint(temp, 4, BLACK);
      dtostrf(powerPara[6], 1, 1, temp);
      strcat(temp, "PF");
      lcdPrint(temp, 5, BLACK);
      dtostrf(sendingDataSet.PARAMETERS[27], 1, 1, temp);
      strcat(temp, "Hz");
#ifdef __GSM__
      if ((!CONNECTION_OPEN) || (!sendDataFlag))lcdPrint(temp, 6, BLACK);
#else
      lcdPrint(temp, 6, BLACK);
#endif

      break;
    case 2:


      nodes(1, WHITE);
      nodes(2, BLACK);
      nodes(3, WHITE);
      strTemp = (String)sendingDataSet.PARAMETERS[13];
      strTemp.toCharArray(temp, 20);
      strcat(temp, "kWh");
      lcdPrint(temp, 1, BLACK);
      dtostrf(bActivePower, 1, 1, temp);
      strcat(temp, "kW");
      lcdPrint(temp, 2, BLACK);
      dtostrf(powerPara[4], 1, 1, temp);
      strcat(temp, "V");
      lcdPrint(temp, 3, BLACK);
      dtostrf(powerPara[1], 1, 1, temp);
      strcat(temp, "A");
      lcdPrint(temp, 4, BLACK);
      dtostrf(powerPara[7], 1, 1, temp);
      strcat(temp, "PF");
      lcdPrint(temp, 5, BLACK);
      dtostrf(sendingDataSet.PARAMETERS[27], 1, 1, temp);
      strcat(temp, "Hz");
#ifdef __GSM__
      if ((!CONNECTION_OPEN) || (!sendDataFlag))lcdPrint(temp, 6, BLACK);
#else
      lcdPrint(temp, 6, BLACK);
#endif

      break;
    case 3:


      nodes(1, WHITE);
      nodes(2, WHITE);
      nodes(3, BLACK);
      strTemp = (String)sendingDataSet.PARAMETERS[14];
      strTemp.toCharArray(temp, 20);
      strcat(temp, "kWh");
      lcdPrint(temp, 1, BLACK);
      dtostrf(cActivePower, 1, 1, temp);
      strcat(temp, "kW");
      lcdPrint(temp, 2, BLACK);
      dtostrf(powerPara[5], 1, 1, temp);
      strcat(temp, "V");
      lcdPrint(temp, 3, BLACK);
      dtostrf(powerPara[2], 1, 1, temp);
      strcat(temp, "A");
      lcdPrint(temp, 4, BLACK);
      dtostrf(powerPara[8], 1, 1, temp);
      strcat(temp, "PF");
      lcdPrint(temp, 5, BLACK);
      dtostrf(sendingDataSet.PARAMETERS[27], 1, 1, temp);
      strcat(temp, "Hz");
#ifdef __GSM__
      if ((!CONNECTION_OPEN) || (!sendDataFlag))lcdPrint(temp, 6, BLACK);
#else
      lcdPrint(temp, 6, BLACK);
#endif
      break;
    case 4:

      nodes(1, BLACK);
      nodes(2, BLACK);
      nodes(3, BLACK);
      strTemp = (String)(sendingDataSet.PARAMETERS[12] + sendingDataSet.PARAMETERS[13] + sendingDataSet.PARAMETERS[14]);
      strTemp.toCharArray(temp, 20);
      strcat(temp, "kWh");
      lcdPrint(temp, 1, BLACK);
      dtostrf((aActivePower + bActivePower + cActivePower), 1, 1, temp);
      strcat(temp, "kW");
      lcdPrint(temp, 2, BLACK);
      dtostrf(((powerPara[3] + powerPara[4] + powerPara[5]) / 3), 1, 1, temp);
      strcat(temp, "V");
      lcdPrint(temp, 3, BLACK);
      dtostrf((powerPara[0] + powerPara[1] + powerPara[2]), 1, 1, temp);
      strcat(temp, "A");
      lcdPrint(temp, 4, BLACK);
      dtostrf(((powerPara[6] + powerPara[7] + powerPara[8]) / 3), 1, 1, temp);
      strcat(temp, "PF");
      lcdPrint(temp, 5, BLACK);
      dtostrf(sendingDataSet.PARAMETERS[27], 1, 1, temp);
      strcat(temp, "Hz");
#ifdef __GSM__
      if ((!CONNECTION_OPEN) || (!sendDataFlag))lcdPrint(temp, 6, BLACK);
#else
      lcdPrint(temp, 6, BLACK);
#endif
      break;
  }


}


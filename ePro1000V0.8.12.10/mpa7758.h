
struct SD_DATA {
  uint32_t SD_COUNTER;
  uint32_t TIME_STAMP;
  uint32_t DEVICE_SN;
  float PARAMETERS[28];
  uint16_t SIGNAL_STRENGTH;
  byte DEVICE_STATE;
  byte CSB;
};


SD_DATA sendingDataSet;

float aReactivePower = 0.0, bReactivePower = 0.0, cReactivePower = 0.0, aActivePower = 0.0, bActivePower = 0.0, cActivePower = 0.0, aApparentPower = 0.0, bApparentPower = 0.0, cApparentPower = 0.0;
long aActiveEnergy = 0, bActiveEnergy = 0, cActiveEnergy = 0, aReactiveEnergy = 0, bReactiveEnergy = 0, cReactiveEnergy = 0, aApparentEnergy = 0, bApparentEnergy = 0, cApparentEnergy = 0;
long aActiveEnergyt = 0, bActiveEnergyt = 0, cActiveEnergyt = 0, aReactiveEnergyt = 0, bReactiveEnergyt = 0, cReactiveEnergyt = 0, aApparentEnergyt = 0, bApparentEnergyt = 0, cApparentEnergyt = 0;
// use t to temporary accumilate Energy
// in arduino double==float only 4 bytes cant have high accuracy thus use long int type here energy = kWh * 10 but max is ~250GWh
// if factory use 1MW 24h 7 day within 1.2 years it will overflow ...


float powerPara[9];

unsigned long blinkCount = 0, secCount = 0; // use volatile for shared variables
int awatthr, bwatthr, cwatthr, avarhr, bvarhr, cvarhr, avahr, bvahr, cvahr, freq, temp;
long airms, birms, cirms, avrms, bvrms, cvrms;




#define AWATTHR   0x01
#define BWATTHR   0x02
#define CWATTHR   0x03
#define AVARHR    0x04
#define BVARHR    0x05
#define CVARHR    0x06
#define AVAHR     0x07
#define BVAHR     0x08
#define CVAHR     0x09
#define AIRMS     0x0A
#define BIRMS     0x0B
#define CIRMS     0x0C
#define AVRMS     0x0D
#define BVRMS     0x0E
#define CVRMS     0x0F
#define FREQ      0x10
#define TEMP      0x11
#define WFORM     0x12
#define OPMODE    0x13
#define MMODE     0x14
#define WAVMODE   0x15
#define COMPMODE  0x16
#define LCYCMODE  0x17
#define MASK      0x18
#define STATUS    0x19
#define RSTATUS   0x1A
#define ZXTOUT    0x1B
#define LINECYC   0x1C
#define SAGCYC    0x1D
#define SAGLVL    0x1E
#define VPINTLVL  0x1F
#define IPINTLVL  0x20
#define VPEAK     0x21
#define IPEAK     0x22
#define GAIN      0x23
#define AVRMSGAIN 0x24
#define BVRMSGAIN 0x25
#define CVRMSGAIN 0x26
#define AIGAIN    0x27
#define BIGAIN    0x28
#define CIGAIN    0x29
#define AWG       0x2A
#define BWG       0x2B
#define CWG       0x2C
#define AVARG     0x2D
#define BVARG     0x2E
#define CVARG     0x2F
#define AVAG      0x30
#define BVAG      0x31
#define CVAG      0x32
#define AVRMSOS   0x33
#define BVRMSOS   0x34
#define CVRMSOS   0x35
#define AIRMSOS   0x36
#define BIRMSOS   0x37
#define CIRMSOS   0x38
#define AWAITOS   0x39
#define BWAITOS   0x3A
#define CWAITOS   0x3B
#define AVAROS    0x3C
#define BVAROS    0x3D
#define CVAROS    0x3E
#define APHCAL    0x3F
#define BPHCAL    0x40
#define CPHCAL    0x41
#define WDIV      0x42
#define VADIV     0x44
#define VARDIV    0x43
#define APCFNUM   0x45
#define APCFDEN   0x46
#define VARCFNUM  0x47
#define VARCFDEN  0x48
#define CHKSUM    0x7E
#define VERSION   0x7F
#define REG_READ(reg) reg
#define REG_WRITE(reg)	(unsigned char)((reg) | 0x80)

void calcVals();
void averageCount();

void enChip()
{
  SPI.setDataMode(SPI_MODE1);//7758 is using mode 1
  digitalWrite(CS7758, LOW);
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  delayMicroseconds(50);
}

void disChip()
{
  digitalWrite(CS7758, HIGH);
  delayMicroseconds(50);
  SPI.setDataMode(SPI_MODE0);//LCD and SD is using mode 0 or 3
}

void bit8write(char reg, unsigned char data)
{
  enChip();

  //    delayMicroseconds(50);
  SPI.transfer(REG_WRITE(reg));
  //    delayMicroseconds(50);
  SPI.transfer((unsigned char)data);
  //    delayMicroseconds(50);

  disChip();
}

void bit16write(char reg, unsigned int data)
{
  enChip();

  delayMicroseconds(50);
  SPI.transfer(REG_WRITE(reg));
  delayMicroseconds(50);
  SPI.transfer((unsigned char)((data >> 8) & 0xFF));
  delayMicroseconds(50);
  SPI.transfer((unsigned char)(data & 0xFF));
  delayMicroseconds(50);

  disChip();
}

void bit24write(char reg, unsigned long data)
{
  enChip();

  delayMicroseconds(50);
  SPI.transfer(REG_WRITE(reg));
  delayMicroseconds(50);
  SPI.transfer((unsigned char)((data >> 16) & 0xFF));
  delayMicroseconds(50);
  SPI.transfer((unsigned char)((data >> 8) & 0xFF));
  delayMicroseconds(50);
  SPI.transfer((unsigned char)(data & 0xFF));
  delayMicroseconds(50);

  disChip();
}

unsigned char bit8read(char reg)
{
  enChip();

  unsigned char mad;
  //    delayMicroseconds(50);
  SPI.transfer(REG_READ(reg));
  //    delayMicroseconds(50);
  mad = SPI.transfer(0xFF);
  //    delayMicroseconds(50);

  disChip();

  return mad;
}

unsigned int bit16read(char reg)
{
  enChip();
  unsigned int mad = 0;
  unsigned char mad0 = 0;
  delayMicroseconds(50);
  SPI.transfer(REG_READ(reg));
  delayMicroseconds(50);
  mad = SPI.transfer(0x00);
  delayMicroseconds(50);
  mad0 = SPI.transfer(0x00);
  delayMicroseconds(50);

  disChip();
  mad = (mad << 8) | mad0;
  return mad;
}

unsigned long bit24read(char reg)
{
  enChip();
  unsigned long mad = 0;
  unsigned int mad1 = 0;
  unsigned char mad0 = 0;
  delayMicroseconds(50);
  SPI.transfer(REG_READ(reg));
  delayMicroseconds(50);
  mad = SPI.transfer(0x00);
  delayMicroseconds(50);
  mad1 = SPI.transfer(0x00);
  delayMicroseconds(50);
  mad0 = SPI.transfer(0x00);
  delayMicroseconds(50);

  disChip();
  mad = (mad << 16) | (mad1 << 8) | mad0;
  return mad;
}

void read7758()
{
  unsigned char bit8;
  unsigned int bit16;
  unsigned long bit24;

  bit16 = bit16read(FREQ);
  sendingDataSet.PARAMETERS[27] = (float) (bit16 / 16.0);
  Serial.println(bit16);
  Serial.println(sendingDataSet.PARAMETERS[27]);
}


//
//void EEPROM_OFFSET_READ() {
//  union {
//    uint16_t a;
//    byte b[2];
//  } bconvf;
//  for (uint16_t i = 0; i < 15; i++) {
//    for (uint16_t j = 0; j < 2; j++) {
//      bconvf.b[j] = EEPROM.read(OFF_SET_EEPROM_START + i * 2 + j);
//    }
//    OFFSET[i] = bconvf.a;
//    Serial.println(OFFSET[i]);
//  }
//}
//
//void EEPROM_OFFSET_WRITE() {
//  union {
//    uint16_t a;
//    byte b[2];
//  } fconvb;
//  for (uint16_t i = 0; i < 15; i++) {
//    fconvb.a = OFFSET[i];
//    for (uint16_t j = 0; j < 2; j++) {
//      EEPROM.write(OFF_SET_EEPROM_START + i * 2 + j, fconvb.b[j]);
//    }
//  }
//
//}

void OFFSET_CONFIG() {
  //  int16_t OFFSET_NEW[15];
  //  File fileOffset;
  //  int sdComma;
  //  int val;
  //  fileOffset = SD.open("offset.txt");
  //  if (fileOffset) {
  //    char in;
  //    String sdText = "";
  //    Serial.print("offset.txt:");
  //    while (fileOffset.available()) {
  //      char in = (fileOffset.read());
  //      sdText += (String)in;
  //    }
  //    Serial.println(sdText);
  //    sdComma = sdText.indexOf(',');
  //    for (int off = 0; off < 15; off++) {
  //      val = sdText.substring(sdComma + 1, sdText.indexOf(',', sdComma + 1)).toInt();
  //      OFFSET_NEW[off] = val;
  //      Serial.println(val);
  //      bit16write(51 + off, val); // 0x33 to 0x3E
  //      sdComma = sdText.indexOf(',', sdComma + 1);
  //    }
  //    union {
  //      uint16_t a;
  //      byte b[2];
  //    } fconvb;
  //    for (uint16_t i = 0; i < 15; i++) {
  //      fconvb.a = OFFSET_NEW[i];
  //      for (uint16_t j = 0; j < 2; j++) {
  //        EEPROM.write(OFF_SET_EEPROM_START + i * 2 + j, fconvb.b[j]);
  //      }
  //    }
  //  }
  //  fileOffset.close();// close the file:
}



void config7758() {
  int16_t OFFSET_OLD[15];
  boolean offsetcheck = false;
  union {
    uint16_t a;
    byte b[2];
  } bconvf;
  for (uint16_t i = 0; i < 15; i++) {
    for (uint16_t j = 0; j < 2; j++) {
      bconvf.b[j] = EEPROM.read(OFF_SET_EEPROM_START + i * 2 + j);
    }
    OFFSET_OLD[i] = bconvf.a;
    Serial.println(OFFSET_OLD[i]);
  }

  for (byte x = 0; x < 15; x++)
    if ((OFFSET_OLD[x] > 0) && (OFFSET_OLD[x] != 0xFFFF))
      offsetcheck = 1;

  if (offsetcheck) {
    for (int off = 0; off < 15; off++) {
      bit16write(51 + off, OFFSET_OLD[off]); // 0x33 to 0x3E
    }
  } else {
    OFFSET_CONFIG();
    for (uint16_t i = 0; i < 15; i++) {
      for (uint16_t j = 0; j < 2; j++) {
        bconvf.b[j] = EEPROM.read(OFF_SET_EEPROM_START + i * 2 + j);
      }
      OFFSET_OLD[i] = bconvf.a;
      Serial.println(OFFSET_OLD[i]);
    }
    for (int off = 0; off < 15; off++) {
      bit16write(51 + off, OFFSET_OLD[off]); // 0x33 to 0x3E
    }
  }
  bit8write(OPMODE, 0x04);
  delayMicroseconds(200);
  bit8write(MMODE, 0xFC);
  delayMicroseconds(200);
  bit8write(WAVMODE, 0x00);
  delayMicroseconds(200);
  bit8write(COMPMODE, 0x9C);
  delayMicroseconds(200);
  bit8write(LCYCMODE, 0x78);
  delayMicroseconds(200);
  bit24write(MASK, 0x000000);
  delayMicroseconds(200);
  bit16write(ZXTOUT, 0xFFFF);
  delayMicroseconds(200);
  bit16write(LINECYC, 0xFFFF);
  delayMicroseconds(200);
  bit8write(SAGCYC, 0xFF);

  delayMicroseconds(200);
  bit8write(SAGLVL, 0x00);
  delayMicroseconds(200);
  bit8write(VPINTLVL, 0xFF);
  delayMicroseconds(200);
  bit8write(IPINTLVL, 0xFF);
  delayMicroseconds(200);

  bit16write(AWG, 0x800);
  delayMicroseconds(200);
  bit16write(BWG, 0x800);
  delayMicroseconds(200);
  bit16write(CWG, 0x800);
  delayMicroseconds(200);

  bit16write(AVAG, 0x800);
  delayMicroseconds(200);
  bit16write(BVAG, 0x800);
  delayMicroseconds(200);
  bit16write(CVAG, 0x800);
  delayMicroseconds(200);

  bit16write(AVARG, 0x800);
  delayMicroseconds(200);
  bit16write(BVARG, 0x800);
  delayMicroseconds(200);
  bit16write(CVARG, 0x800);
  delayMicroseconds(200);


}

float powerFactorCal(float pf) {

  if (1.0 <= pf) {
    return 0.98;
  } else if (-1.0 >= pf) {
    return -0.98;
  } else return pf;
}

void myAccumilate(long * msb, long * lsb)
{
  if (*lsb < 0)
  {
    *msb += (*lsb >> 16) + 1;
    *lsb = *lsb | 0xFFFF0000;
  }
  else
  {
    *msb += (*lsb >> 16);
    *lsb = *lsb & 0x0000FFFF;
  }
}

void intRead7758()
{
  long tempIRms = 0;
  time = micros();
  awatthr = bit16read(AWATTHR);
  bwatthr = bit16read(BWATTHR);
  cwatthr = bit16read(CWATTHR);
  avarhr = bit16read(AVARHR);
  bvarhr = bit16read(BVARHR);
  cvarhr = bit16read(CVARHR);
  avahr = bit16read(AVAHR);
  bvahr = bit16read(BVAHR);
  cvahr = bit16read(CVAHR);
  freq = bit16read(FREQ);

  temp = bit8read(TEMP);
  tempIRms = bit24read(AIRMS);
  airms = (tempIRms < CURRENT_THRESHOLD) ? 0 : tempIRms;
  tempIRms = bit24read(BIRMS);
  birms = (tempIRms < CURRENT_THRESHOLD) ? 0 : tempIRms;
  tempIRms = bit24read(CIRMS);
  cirms = (tempIRms < CURRENT_THRESHOLD) ? 0 : tempIRms;
  
  avrms = bit24read(AVRMS);
  avrms = (bitRead(avrms, 23) == 1) ? 0 : avrms;
  bvrms = bit24read(BVRMS);
  bvrms = (bitRead(bvrms, 23) == 1) ? 0 : bvrms;
  cvrms = bit24read(CVRMS);
  cvrms = (bitRead(cvrms, 23) == 1) ? 0 : cvrms;


  calcVals();

}

void calcVals()
{

  float deltaTime = 0;
  deltaTime = (float) (time - oldTime);

  sendingDataSet.PARAMETERS[27] = (float) (freq / 16.0);


  powerPara[3] = (float) (avrms / K[7][0]);
  powerPara[4] = (float) (bvrms / K[7][1]);
  powerPara[5] = (float) (cvrms / K[7][2]);

  powerPara[0] = (float) (airms / K[6][0]);
  powerPara[1] = (float) (birms / K[6][1]);
  powerPara[2] = (float) (cirms / K[6][2]);

  aActivePower =  (((float)awatthr * K[1][0]) / deltaTime) * 1000;
  bActivePower =  (((float)bwatthr * K[1][1]) / deltaTime) * 1000;
  cActivePower =  (((float)cwatthr * K[1][2]) / deltaTime) * 1000;

  aReactivePower = (((float)avarhr * K[0][0]) / deltaTime) * 1000;
  bReactivePower = (((float)bvarhr * K[0][1]) / deltaTime) * 1000;
  cReactivePower = (((float)cvarhr * K[0][2]) / deltaTime) * 1000;

  aApparentPower = (((float)avahr * K[2][0]) / deltaTime) * 1000;
  bApparentPower = (((float)bvahr * K[2][1]) / deltaTime) * 1000;
  cApparentPower = (((float)cvahr * K[2][2]) / deltaTime) * 1000;

  powerPara[6] = (float) powerFactorCal((aActivePower / aApparentPower));
  powerPara[7] = (float) powerFactorCal((bActivePower / bApparentPower));
  powerPara[8] = (float) powerFactorCal((cActivePower / cApparentPower));

  aActiveEnergyt += (long)(awatthr);
  bActiveEnergyt += (long)(bwatthr);
  cActiveEnergyt += (long)(cwatthr);

  aReactiveEnergyt += (long)(avarhr);
  bReactiveEnergyt += (long)(bvarhr);
  cReactiveEnergyt += (long)(cvarhr);


  aApparentEnergyt += (long)(avahr);
  bApparentEnergyt += (long)(bvahr);
  cApparentEnergyt += (long)(cvahr);

  myAccumilate(& aActiveEnergy, & aActiveEnergyt);
  myAccumilate(& bActiveEnergy, & bActiveEnergyt);
  myAccumilate(& cActiveEnergy, & cActiveEnergyt);

  myAccumilate(& aReactiveEnergy, & aReactiveEnergyt);
  myAccumilate(& bReactiveEnergy, & bReactiveEnergyt);
  myAccumilate(& cReactiveEnergy, & cReactiveEnergyt);

  myAccumilate(& aApparentEnergy, & aApparentEnergyt);
  myAccumilate(& bApparentEnergy, & bApparentEnergyt);
  myAccumilate(& cApparentEnergy, & cApparentEnergyt);

  sendingDataSet.PARAMETERS[12] =  (float)aActiveEnergy * K[4][0];
  sendingDataSet.PARAMETERS[13] =  (float)bActiveEnergy * K[4][1];
  sendingDataSet.PARAMETERS[14] =  (float)cActiveEnergy * K[4][2];

  sendingDataSet.PARAMETERS[9] =  (float)aReactiveEnergy * K[3][0];
  sendingDataSet.PARAMETERS[10] =  (float)bReactiveEnergy * K[3][1];
  sendingDataSet.PARAMETERS[11] =  (float)cReactiveEnergy * K[3][2];

  sendingDataSet.PARAMETERS[15] =  (float)aApparentEnergy * K[5][0];
  sendingDataSet.PARAMETERS[16] =  (float)bApparentEnergy * K[5][1];
  sendingDataSet.PARAMETERS[17] =  (float)cApparentEnergy * K[5][2];

  averageCount();

  oldTime = time;


}

void averageCount() {

  if (fiveMinCounter > 0) {

    sendingDataSet.PARAMETERS[21] += powerPara[3];
    sendingDataSet.PARAMETERS[22] += powerPara[4];
    sendingDataSet.PARAMETERS[23] += powerPara[5];

    sendingDataSet.PARAMETERS[18] += powerPara[0];
    sendingDataSet.PARAMETERS[19] += powerPara[1];
    sendingDataSet.PARAMETERS[20] += powerPara[2];

    sendingDataSet.PARAMETERS[3] +=  aActivePower;
    sendingDataSet.PARAMETERS[4] +=  bActivePower;
    sendingDataSet.PARAMETERS[5] +=  cActivePower;

    sendingDataSet.PARAMETERS[0] += aReactivePower;
    sendingDataSet.PARAMETERS[1] += bReactivePower;
    sendingDataSet.PARAMETERS[2] += cReactivePower;

    sendingDataSet.PARAMETERS[6] += aApparentPower;
    sendingDataSet.PARAMETERS[7] += bApparentPower;
    sendingDataSet.PARAMETERS[8] += cApparentPower;

    sendingDataSet.PARAMETERS[24] += powerPara[6];
    sendingDataSet.PARAMETERS[25] += powerPara[7];
    sendingDataSet.PARAMETERS[26] += powerPara[8];
  } else {

    sendingDataSet.PARAMETERS[21] = powerPara[3];
    sendingDataSet.PARAMETERS[22] = powerPara[4];
    sendingDataSet.PARAMETERS[23] = powerPara[5];

    sendingDataSet.PARAMETERS[18] = powerPara[0];
    sendingDataSet.PARAMETERS[19] = powerPara[1];
    sendingDataSet.PARAMETERS[20] = powerPara[2];

    sendingDataSet.PARAMETERS[3] =  aActivePower;
    sendingDataSet.PARAMETERS[4] =  bActivePower;
    sendingDataSet.PARAMETERS[5] =  cActivePower;

    sendingDataSet.PARAMETERS[0] = aReactivePower;
    sendingDataSet.PARAMETERS[1] = bReactivePower;
    sendingDataSet.PARAMETERS[2] = cReactivePower;

    sendingDataSet.PARAMETERS[6] = aApparentPower;
    sendingDataSet.PARAMETERS[7] = bApparentPower;
    sendingDataSet.PARAMETERS[8] = cApparentPower;

    sendingDataSet.PARAMETERS[24] = powerPara[6];
    sendingDataSet.PARAMETERS[25] = powerPara[7];
    sendingDataSet.PARAMETERS[26] = powerPara[8];
  }
  fiveMinCounter++;
}
void averageSec() {
  sendingDataSet.PARAMETERS[21] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[22] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[23] /= (float) fiveMinCounter;

  sendingDataSet.PARAMETERS[18] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[19] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[20] /= (float) fiveMinCounter;


  sendingDataSet.PARAMETERS[3] /=  (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[4] /=  (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[5] /=  (float) fiveMinCounter;

  sendingDataSet.PARAMETERS[0] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[1] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[2] /= (float) fiveMinCounter;

  sendingDataSet.PARAMETERS[6] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[7] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[8] /= (float) fiveMinCounter;

  sendingDataSet.PARAMETERS[24] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[25] /= (float) fiveMinCounter;
  sendingDataSet.PARAMETERS[26] /= (float) fiveMinCounter;

  fiveMinCounter = 0;
}




void readAll()
{

  unsigned char bit8;
  unsigned int bit16;
  unsigned long bit24;

  Serial.println("Start reading all regs ################ ");

  bit16 = bit16read(0x01);
  Serial.print("0x01=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x02);
  Serial.print("0x02=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x03);
  Serial.print("0x03=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x04);
  Serial.print("0x04=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x05);
  Serial.print("0x05=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x06);
  Serial.print("0x06=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x07);
  Serial.print("0x07=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x08);
  Serial.print("0x08=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x09);
  Serial.print("0x09=");
  Serial.println(bit16, HEX);

  bit24 = bit24read(0x0A);
  Serial.print("0x0A=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x0B);
  Serial.print("0x0B=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x0C);
  Serial.print("0x0C=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x0D);
  Serial.print("0x0D=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x0E);
  Serial.print("0x0E=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x0F);
  Serial.print("0x0F=");
  Serial.println(bit24, HEX);

  bit16 = bit16read(0x10);
  Serial.print("0x10=");
  Serial.println(bit16, HEX);

  bit8 = bit8read(0x11);
  Serial.print("0x11=");
  Serial.println(bit8, HEX);

  bit24 = bit24read(0x12);
  Serial.print("0x12=");
  Serial.println(bit24, HEX);



  bit8 = bit8read(0x13);
  Serial.print("0x13=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x14);
  Serial.print("0x14=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x15);
  Serial.print("0x15=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x16);
  Serial.print("0x16=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x17);
  Serial.print("0x17=");
  Serial.println(bit8, HEX);

  bit24 = bit24read(0x18);
  Serial.print("0x18=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x19);
  Serial.print("0x19=");
  Serial.println(bit24, HEX);

  bit24 = bit24read(0x1A);
  Serial.print("0x1A=");
  Serial.println(bit24, HEX);


  bit16 = bit16read(0x1B);
  Serial.print("0x1B=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x1C);
  Serial.print("0x1C=");
  Serial.println(bit16, HEX);

  bit8 = bit8read(0x1D);
  Serial.print("0x1D=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x1E);
  Serial.print("0x1E=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x1F);
  Serial.print("0x1F=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x20);
  Serial.print("0x20=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x21);
  Serial.print("0x21=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x22);
  Serial.print("0x22=");
  Serial.println(bit8, HEX);


  bit8 = bit8read(0x23);
  Serial.print("0x23=");
  Serial.println(bit8, HEX);




  bit16 = bit16read(0x24);
  Serial.print("0x24=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x25);
  Serial.print("0x25=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x26);
  Serial.print("0x26=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x27);
  Serial.print("0x27=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x28);
  Serial.print("0x28=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x29);
  Serial.print("0x29=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x2A);
  Serial.print("0x2A=");
  Serial.println(bit16, HEX);




  bit16 = bit16read(0x2B);
  Serial.print("0x2B=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x2C);
  Serial.print("0x2C=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x2D);
  Serial.print("0x2D=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x2E);
  Serial.print("0x2E=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x2F);
  Serial.print("0x2F=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x30);
  Serial.print("0x30=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x31);
  Serial.print("0x31=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x32);
  Serial.print("0x32=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x33);
  Serial.print("0x33=");
  Serial.println(bit16, HEX);



  bit16 = bit16read(0x34);
  Serial.print("0x34=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x35);
  Serial.print("0x35=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x36);
  Serial.print("0x36=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x37);
  Serial.print("0x37=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x38);
  Serial.print("0x38=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x39);
  Serial.print("0x39=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x3A);
  Serial.print("0x3A=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x3B);
  Serial.print("0x3B=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x3C);
  Serial.print("0x3C=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x3D);
  Serial.print("0x3D=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x3E);
  Serial.print("0x3E=");
  Serial.println(bit16, HEX);


  bit8 = bit8read(0x3F);
  Serial.print("0x3F=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x40);
  Serial.print("0x40=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x41);
  Serial.print("0x41=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x42);
  Serial.print("0x42=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x43);
  Serial.print("0x43=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x44);
  Serial.print("0x44=");
  Serial.println(bit8, HEX);

  bit16 = bit16read(0x45);
  Serial.print("0x45=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x46);
  Serial.print("0x46=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x47);
  Serial.print("0x47=");
  Serial.println(bit16, HEX);

  bit16 = bit16read(0x48);
  Serial.print("0x48=");
  Serial.println(bit16, HEX);

  bit8 = bit8read(0x7E);
  Serial.print("0x7E=");
  Serial.println(bit8, HEX);

  bit8 = bit8read(0x7F);
  Serial.print("0x7F=");
  Serial.println(bit8, HEX);




}// end of readAll



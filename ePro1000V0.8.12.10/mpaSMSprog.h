
char sendATcmd(char* ATcommand, String expected_answer1, unsigned int timeout);

void sendSMS(String message, String pn)
{
  char buff[100];
  char pnbuff[15];
  msgPrint("SMS INIT");
  if (sendATcmd("AT+CMGF=1\r", "OK", 500)) { // AT command to send SMS message
    pn.toCharArray(pnbuff, pn.length() + 1);
    sprintf(buff, "AT + CMGS = \"%s\"\r" , pnbuff); // "OK"
    if (sendATcmd(buff, ">", 500)) {
      Serial3.print(message); // message to send
      char a[1] = {0x1a};
      msgPrint("SMS INIT.");
      sendATcmd(a, "+CMGS:", 5000);
    } else {
      char a[1] = {0x1a};
      msgPrint("SMS INIT.");
      sendATcmd(a, "OK", 15000);
    }
  }
  Serial3.println();
  
  msgPrint("SMS INIT..");
  delay(1500);
  msgPrint("SMS INIT...");
  delay(1500);
  msgPrint("SMS INIT....");
  delay(1500);
  msgPrint("SMS INIT.....");
  delay(1500);
}


char msgOperation() {
  char ret = 0;
  String sms = smsStore;
  int comma[10];
  unsigned long CRN;


  if ((phoneNumber.indexOf(configPhoneNum1) >= 0) || (phoneNumber.indexOf(configPhoneNum2) >= 0)) {
    ret = 1;
    Serial.println("SMS Operation OK");
    if (sms.startsWith("SET CRN")) {
      ret = 2;
      CRN = sms.substring(sms.indexOf(':') + 1).toInt();;
      EEPROM_CRN_WRITE(CRN);
      sendingDataSet.DEVICE_SN = CRN;
    } else if (sms.startsWith("RESTART")) {
      ret = 3;
      wdt_enable(WDTO_4S);
    } else if (sms.startsWith("RESET CRN")) {
      ret = 4;
      aActiveEnergy = 0;
      bActiveEnergy = 0;
      cActiveEnergy = 0;
      aReactiveEnergy = 0;
      bReactiveEnergy = 0;
      cReactiveEnergy = 0;
      aApparentEnergy = 0;
      bApparentEnergy = 0;
      cApparentEnergy = 0;
      CRN = sms.substring(sms.indexOf(':') + 1).toInt();
      EEPROM_CRN_WRITE(CRN);
      sendingDataSet.DEVICE_SN = CRN;
      OFFSET_CONFIG();
      K_READ_SD();
      energyEEWrite();
      wdt_enable(WDTO_4S);
      //      wdt_enable(WDTO_2S);
    } else if (sms.startsWith("RED")) {
      ret = 5;
      for (byte x = 0; x < 9; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
    else if (sms.startsWith("YELLOW")) {
      ret = 6;
      for (byte x = 0; x < 8; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
    else if (sms.startsWith("BLUE")) {
      ret = 7;
      for (byte x = 0; x < 8; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
  }
  return ret;
}




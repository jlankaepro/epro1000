#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)
#define DAYS_PER_WEEK (7L)
#define SECS_PER_WEEK (SECS_PER_DAY * DAYS_PER_WEEK)
#define SECS_PER_YEAR (SECS_PER_WEEK * 52L)
#define SECS_YR_2000  (946681200UL)
#define SECONDS_FROM_1970_TO_2000 946684800UL

typedef struct epoch {
  uint8_t Second;
  uint8_t Minute;
  uint8_t Hour;
  uint8_t Day;
  uint8_t Month;
  uint16_t Year;
} TIME_CONVERT;
TIME_CONVERT SIM900_TIME;

static  const uint8_t monthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void DateTime (uint32_t t);
void myTimeCreater();
uint32_t makeTime();



void getDateTime() {
  String dateTime1 = "";
  char temp[100];
#ifdef __GSM__
  responceReader(2, temp);
#endif
  dateTime1 = (String)temp;

  if ((dateTime1.indexOf("+CCLK:") == 0) && (dateTime1.substring(25, 26) == "+") ) {
    localRTC = 0;
    if (dateTime1.substring(16, 17) == ",") {
      SIM900_TIME.Year = 2000 + dateTime1.substring(8, 10).toInt();
      SIM900_TIME.Month = dateTime1.substring(11, 13).toInt();
      SIM900_TIME.Day = dateTime1.substring(14, 16).toInt();
    }
    if ((dateTime1.substring(16, 17) == ",") && ((dateTime1.substring(25, 26) == "+"))) {
      SIM900_TIME.Hour = dateTime1.substring(17, 19).toInt();
      SIM900_TIME.Minute = dateTime1.substring(20, 22).toInt();
      SIM900_TIME.Second = dateTime1.substring(23, 25).toInt();
    }
    myTimeCreater();
    localRTC = makeTime();
  }
  dateTime1 = "";
}

uint8_t signalStrength() {
  uint8_t ret = 0;
  String CSQ = "";
  char temp[100];

#ifdef __GSM__
  responceReader(1, temp);
#endif
  CSQ =  String(temp);
  Serial.println(CSQ);
  ret = CSQ.substring(5, CSQ.indexOf(",")).toInt();
  Serial.println(ret);
  CSQ = "";
  return ret;
}

uint32_t makeTime() {

  int i;
  uint32_t seconds;


  seconds = (SIM900_TIME.Year - 1970) * (60 * 60 * 24L * 365);
  for (i = 1970; i < SIM900_TIME.Year; i++) {
    if ((i % 4) == 0) {
      seconds += 60 * 60 * 24L;
    }
  }


  for (i = 1; i < SIM900_TIME.Month; i++) {
    if ( (i == 2) && ((SIM900_TIME.Year % 4) == 0)) {
      seconds += 60 * 60 * 24L * 29;
    } else {
      seconds += 60 * 60 * 24L * monthDays[i - 1];
    }
  }
  seconds += (SIM900_TIME.Day - 1) * 3600 * 24L;
  seconds += SIM900_TIME.Hour * 3600L;
  seconds += SIM900_TIME.Minute * 60L;
  seconds += SIM900_TIME.Second;
  return seconds - 19800UL;
}

void myTimeCreater() {
  myTime = SIM900_TIME.Year;
  if(SIM900_TIME.Month >= 10) myTime += "/";
  else myTime += "/0";
  myTime += SIM900_TIME.Month;
  if(SIM900_TIME.Day >= 10) myTime += "/";
  else myTime += "/0";
  myTime += SIM900_TIME.Day;
  if(SIM900_TIME.Hour >= 10) myTime += " ";
  else myTime += " 0";
  myTime += SIM900_TIME.Hour;
  if(SIM900_TIME.Minute >= 10) myTime += ":";
  else myTime += ":0";
  myTime += SIM900_TIME.Minute;
  if(SIM900_TIME.Second >= 10) myTime += ":";
  else myTime += ":0";
  myTime += SIM900_TIME.Second;
}


void DateTime (uint32_t t) {
  t -= SECONDS_FROM_1970_TO_2000;    // bring to 2000 timestamp from 1970

  SIM900_TIME.Second = t % 60;
  t /= 60;
  SIM900_TIME.Minute = t % 60;
  t /= 60;
  SIM900_TIME.Hour = t % 24;
  uint16_t days = t / 24;
  uint8_t leap;
  for (SIM900_TIME.Year = 0; ; ++SIM900_TIME.Year) {
    leap = SIM900_TIME.Year % 4 == 0;
    if (days < 365 + leap)
      break;
    days -= 365 + leap;
  }
  for (SIM900_TIME.Month = 1; ; ++SIM900_TIME.Month) {
    uint8_t daysPerMonth = monthDays[SIM900_TIME.Month - 1];
    if (leap && SIM900_TIME.Month == 2)
      ++daysPerMonth;
    if (days < daysPerMonth)
      break;
    days -= daysPerMonth;
  }
  SIM900_TIME.Day = days + 1;
  SIM900_TIME.Year += 2000;
  myTimeCreater();
}



char gprsSendSetup() {
  char buff[100];
  if (AT_STATE[25] == -3) {
    for (int x = 6; x < 50; x++)AT_STATE[x] = -1;
  }
  if (ATCondition(3)) {
    sendAT("AT\r\n", 0, 3, 500, 100, 0, 2);
    //    AT_STATE[1] = -1;
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(4)) {
      sendAT("AT+CREG=1\r\n", 0, 4, 500, 100, 0, 2);
    }
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(5)) {
      sendAT("AT +CREG?\r\n", 13, 5, 500, 100, 0, 2);
    }
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(6, 15)) {
      sendRangeAT("AT +CIPSTATUS", 1, 6, 10, 500, 100, 0, 2); //"IP INITIAL"
      Serial.println("AT +CIPSTATUS");
    }
  }

  if (AT_STATE_READER(7)) {
    if (ATCondition(23)) {
      sendAT("AT +CIPCLOSE = 1\r\n", 11, 23, 500, 100, 0, 3); // "CLOSE OK"
    }
  }

  if ((AT_STATE_READER(11)) || (AT_STATE_READER(14))) {
    if (ATCondition(25)) {
      sendAT("AT +CIPSHUT\r\n", 23, 25, 500, 100, 0, 3); // "CLOSE OK"
    }
  }

  if (AT_STATE_READER(6)) {
    if (ATCondition(15)) {
      sendAT("AT +CIPMUX=0\r\n", 0, 15, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(16)) {
      sendAT("AT+CIPMODE=1\r\n", 0, 16, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(24)) {
      sendAT("AT+CGATT =1\r\n", 0, 24, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(17)) {
      sendAT("AT+CGATT?\r\n", 0, 17, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(18)) {
      sprintf(buff, "AT +CSTT=\"%s\"\r\n", APN);// "OK"
      sendAT(buff, 0, 18, 500, 100, 0, 3);
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(10))) {
    if (ATCondition(19)) {
      sendAT("AT +CIICR\r\n", 0, 19, 30000, 200, 0, 1); // "OK"
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(9)) || (AT_STATE_READER(10))) {
    if (ATCondition(20)) {
      sendAT("AT +CIFSR\r\n", 16, 20, 500, 100, 1, 3);
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(7)) || (AT_STATE_READER(13)) || (AT_STATE_READER(8)) || (AT_STATE_READER(9)) || (AT_STATE_READER(10))) {
    if (ATCondition(21)) {
      sprintf(buff, "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n", IP, port);
      sendAT(buff, 22, 21, 30000, 100, 0, 1);
    }
  }
}

void sendRemainData() {


  String SrvResp = "";

  uint16_t NO_OF_PACKET = 0;
  uint32_t POSISION_OF_SD = 0;
  char temp[100];
  responceReader(26,temp);
  SrvResp = String(temp);
  if (SrvResp.indexOf("SERVER:") >= 0) {
    
    Serial.println(SrvResp);
    POSISION_OF_SD = SrvResp.substring(SrvResp.indexOf("SERVER:") + 7, SrvResp.indexOf(',')).toInt();
    Serial.println(POSISION_OF_SD);
    NO_OF_PACKET = SrvResp.substring(SrvResp.indexOf(',') + 1).toInt();
    if (NO_OF_PACKET > 30) NO_OF_PACKET = 30;
    Serial.println(NO_OF_PACKET);
    for (byte z = 0; z < NO_OF_PACKET; z++) {
      wdt_reset();
      byte sendRemainDataChar[512] = {0};
      if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[0] + (POSISION_OF_SD % 262144) + z), sendRemainDataChar) == 0) {
        //        uint32_t(DEVICE_SD_START[0] + (sendingDataSet.SD_COUNTER % 262144)
        SrvResp = "";
        for (int con = 0; con < 512; con++) {
          char tempChar = (char)sendRemainDataChar[con];
          if (tempChar != DECRYPTOR) {
            SrvResp += tempChar;
          } else {
            SrvResp += tempChar;
            break;
          }
        }
        SrvResp = SrvResp.substring(0, SrvResp.indexOf(DECRYPTOR) + 1);
        Serial3.print(SrvResp);
        Serial.print(SrvResp);
      }
      Serial3.println();
      Serial.println();
    }
  }
  SrvResp = "";
}

void TCP_CLOSE() {

  digitalWrite(DTR, HIGH);
  digitalWrite(DTR, LOW);
  delay(1000);
  digitalWrite(DTR, HIGH);
  if ((AT_STATE[27] < 0) && (!ATFlag) && (AT_STATE[27] > -3)) {
    sendAT("+++", 25, 27, 1700, 200, 0, 4);

    Serial.print("+++");
  }
}

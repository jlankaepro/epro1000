const char state_0[] PROGMEM = "OK";
const char state_1[] PROGMEM = "STATE: IP INITIAL";//6
const char state_2[] PROGMEM = "STATE: CONNECT OK";//7
const char state_3[] PROGMEM = "STATE: IP STATUS";//8
const char state_4[] PROGMEM = "STATE: IP GPRSACT";//9
const char state_5[] PROGMEM = "STATE: IP START";//10
const char state_6[] PROGMEM = "STATE: PDP DEACT";//11
const char state_7[] PROGMEM = "ALREADY CONNECT";//12
const char state_8[] PROGMEM = "STATE: TCP CLOSED";//13
const char state_9[] PROGMEM = "STATE: IP CONFIG";//14
const char state_10[] PROGMEM = "STATE: TCP CONNECTING";//15
const char state_11[] PROGMEM = "CLOSE OK";
const char state_12[] PROGMEM = "+CPIN: READY";
const char state_13[] PROGMEM = "+CREG: 1,1";
const char state_14[] PROGMEM = "+CGACT: 1,0";
const char state_15[] PROGMEM = "+CGACT: 1,1";
const char state_16[] PROGMEM = ".";
const char state_17[] PROGMEM = "+CIPSEND:";
const char state_18[] PROGMEM = "SEND OK";
const char state_19[] PROGMEM = "ERROR";
const char state_20[] PROGMEM = "+CCLK: ";
const char state_21[] PROGMEM = "+CSQ: ";
const char state_22[] PROGMEM = "CONNECT";
const char state_23[] PROGMEM = "SHUT OK";
const char state_24[] PROGMEM = "SERVER:";
const char state_25[] PROGMEM = "+++";
const char state_26[] PROGMEM = "OK";
const char state_27[] PROGMEM = "OK";
const char state_28[] PROGMEM = ">";
const char state_29[] PROGMEM = "HAVE RESTART MESSAGE";
const char state_30[] PROGMEM = "+CMGR:";
const char state_31[] PROGMEM = "OK";
const char state_32[] PROGMEM = "String 5";
const char state_33[] PROGMEM = "String 5";
const char state_34[] PROGMEM = "String 5";
const char state_35[] PROGMEM = "String 5";
const char state_36[] PROGMEM = "String 5";
const char state_37[] PROGMEM = "String 5";
const char state_38[] PROGMEM = "String 5";
const char state_39[] PROGMEM = "String 5";
const char state_40[] PROGMEM = "String 5";
const char state_41[] PROGMEM = "String 5";
const char state_42[] PROGMEM = "String 5";
const char state_43[] PROGMEM = "String 5";
const char state_44[] PROGMEM = "String 5";
const char state_45[] PROGMEM = "String 5";
const char state_46[] PROGMEM = "String 5";
const char state_47[] PROGMEM = "String 5";
const char state_48[] PROGMEM = "String 5";
const char state_49[] PROGMEM = "String 5";
const char state_50[] PROGMEM = "String 5";
const char state_51[] PROGMEM = "String 5";
const char state_52[] PROGMEM = "String 5";
const char state_53[] PROGMEM = "String 5";
const char state_54[] PROGMEM = "String 5";
const char state_55[] PROGMEM = "String 5";
const char state_56[] PROGMEM = "String 5";
const char state_57[] PROGMEM = "String 5";
const char state_58[] PROGMEM = "String 5";
const char state_59[] PROGMEM = "String 5";
const char state_60[] PROGMEM = "String 5";
const char state_61[] PROGMEM = "String 5";
const char state_62[] PROGMEM = "String 5";
const char state_63[] PROGMEM = "String 5";
const char state_64[] PROGMEM = "String 5";
const char state_65[] PROGMEM = "String 5";
const char state_66[] PROGMEM = "String 5";
const char state_67[] PROGMEM = "String 5";
const char state_68[] PROGMEM = "String 5";
const char state_69[] PROGMEM = "String 5";
const char state_70[] PROGMEM = "String 5";
const char state_71[] PROGMEM = "String 5";
const char state_72[] PROGMEM = "String 5";
const char state_73[] PROGMEM = "String 5";
const char state_74[] PROGMEM = "String 5";
const char state_75[] PROGMEM = "String 5";
const char state_76[] PROGMEM = "String 5";
const char state_77[] PROGMEM = "String 5";
const char state_78[] PROGMEM = "String 5";
const char state_79[] PROGMEM = "String 5";
const char state_80[] PROGMEM = "String 5";
const char state_81[] PROGMEM = "String 5";
const char state_82[] PROGMEM = "String 5";
const char state_83[] PROGMEM = "String 5";
const char state_84[] PROGMEM = "String 5";
const char state_85[] PROGMEM = "String 5";
const char state_86[] PROGMEM = "String 5";
const char state_87[] PROGMEM = "String 5";
const char state_88[] PROGMEM = "String 5";
const char state_89[] PROGMEM = "String 5";
const char state_90[] PROGMEM = "String 5";
const char state_91[] PROGMEM = "String 5";
const char state_92[] PROGMEM = "String 5";
const char state_93[] PROGMEM = "String 5";
const char state_94[] PROGMEM = "String 5";
const char state_95[] PROGMEM = "String 5";
const char state_96[] PROGMEM = "String 5";
const char state_97[] PROGMEM = "String 5";
const char state_98[] PROGMEM = "String 5";
const char state_99[] PROGMEM = "String 5";



const char* const state_table[] PROGMEM = {
                                            state_0, state_1, state_2, state_3, state_4, state_5, state_6, state_7, state_8, state_9, 
                                            state_10, state_11, state_12, state_13, state_14, state_15, state_16, state_17, 
                                            state_18, state_19, state_20, state_21, state_22, state_23, state_24, state_25, 
                                            state_26, state_27, state_28, state_29, state_30, state_31, state_32, state_33, 
                                            state_34, state_35, state_36, state_37, state_38, state_39, state_40, state_41, 
                                            state_42, state_43, state_44, state_45, state_46, state_47, state_48, state_49, 
                                            state_50, state_51, state_52, state_53, state_54, state_55, state_56, state_57, 
                                            state_58, state_59, state_60, state_61, state_62, state_63, state_64, state_65, 
                                            state_66, state_67, state_68, state_69, state_70, state_71, state_72, state_73, 
                                            state_74, state_75, state_76, state_77, state_78, state_79, state_80, state_81, 
                                            state_82, state_83, state_84, state_85, state_86, state_87, state_88, state_89, 
                                            state_90, state_91, state_92, state_93, state_94, state_95, state_96, state_97, 
                                            state_98, state_99
                                           };

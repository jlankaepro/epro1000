#include <EEPROM.h>

#include <TimerOne.h>

#include <avr/pgmspace.h>

#include <SPI.h>

#include <avr/wdt.h>

#include "config/version.h"
#include "config/config.h"

const uint32_t DEVICE_SD_START[] = {524288, 786432, 1048576, 1310720, 1572864, 1835008, 2097152, 2359296, 2621440, 2883584};
const uint32_t ERROR_COUNTER_SATRT = 262144;
const String RESTART_CLAUSES[] = {"Power-on Reset", "External Reset", "Brown-out Reset", "Watchdog Reset", "JTAG Reset", "POWER ON"};

#define MAX_EEPROM_ENDURANCE 78
#define SD_CLUSTER_START 1000
#define MAX_GSM_ERROR_THRESHOLD 50
#define K_EEPROM_START 4000 // End with after 96 bytes 
#define CRN_EEPROM_START 3926 // End with after 4 bytes
#define OFF_SET_EEPROM_START 3970 // End with after 30 bytes 
#define DEVICE_IDS_EEPROM_START 3926//End with after 24 bytes  
#define TCP_SEND_BUFF_SIZE 1024
#define TCP_SEND_SIGNAL_STRENGTH 8
#define oneMinute 60000
#define TCP_MSG_SIZE  400
#define MASTER_STATE 0
#define SENDING_DATA_PERIOD 5
#define MCUSR_EEPROM_LOC 3900

#define CURRENT_THRESHOLD 1000

#define ENCRYPTOR 110 //n

#define DECRYPTOR 10

#define __GSM__ // if need to shut down GSM module comment <-- this 

//#define DEBUG_MPA

float K[8][3] = {0.0};

int fiveMinCounter = 0, restartCounter = 0;
byte gsmErrorCounter = 0, tempGSM = 0, ipConnectingCounter = 0, restartFlagCounter = 0;
String myTime = "1111/11/11 11:11:11";
unsigned long time = 0, inte15Sec = 0, oldTime = 0, inte500ms = 0, inte100ms = 0;
byte micTemp = 0, micCount = 0;
boolean lcdBackLight = true, writeDataSDFlag = false;
byte myMin = 0, my5Sec = 0;
uint32_t errorCounter, localRTC = 946684800UL;





#ifdef __GSM__

String GENARAL_RES = "";
uint32_t ATtimer[2];
boolean ATFlag = false, responceFlag = false, sendDataFlag = false, revSMSFlag = false, readSMSFlag = false;
byte AT_RESPONCE_FLAG[3];
String AT_RESPONCE[3];
boolean TCP_CONNECT_FLAG = false, CONNECTION_OPEN = false;
char AT_STATE[100];
int8_t AT_STATE_ATTEMPT[100];
byte AT_STATE_DUPLICATE = -1;
String smsStore = "";
String phoneNumber = "";
byte SMSposition = 0, SMS_STORE_COUNTER = 0;;

#endif

void msgPrint(char error[]);
void errorPrint(char error[]);

#include "pin.h"
#include "SDCARD.h"
#include "mpa7758.h"
#include "states.h"
#include "information.h"
#include "mpaNokia5110.h"
#include "mpaEEPROM.h"
#include "displayData.h"

#ifdef __GSM__

#include "mpaSMSprog.h"
#include "mpaGSM_prog.h"
#include "mpagprs.h"

#endif

#include "mpaTime.h"


void setupInt(void) {
  Timer1.initialize(5053302);// max period 8388608 us (8.3... sec)
  Timer1.attachInterrupt(myIsr); // isr run every 5 seconds
}

void myIsr(void) {
  my5Sec++;
  if (sendingDataSet.TIME_STAMP == localRTC) { //  If networktime not recieve call DateTime function with local RTC
    DateTime (localRTC); // in this function make String for Display
  }
  localRTC += 5;
  sendingDataSet.TIME_STAMP = localRTC;
  if (my5Sec >= 12) {
    myMin++;
    restartCounter++;
    writeDataSD();
    my5Sec = 0;
  }

#ifdef __GSM__
  if (myMin >= SENDING_DATA_PERIOD) {
    sendTCPData();
    myMin = 0;
  }
#endif
}

void writeDataSD() {
  writeDataSDFlag = true;
}

#ifdef __GSM__
void sendTCPData() {
  for (int x = 3; x < 50; x++)AT_STATE[x] = -1;
  sendDataFlag = true;
  ipConnectingCounter++;
}
#endif



void setup() {

  pinSetup();

  SPI.begin();

  SPI.setDataMode(SPI_MODE0);// for SD and LCD
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  SPI.setBitOrder(MSBFIRST);
  Serial.begin(115200);
  Serial3.begin(9600);

  Serial.println(F("Starting the Prog"));
  Serial.println(VER);
  Serial.println(F("Jlanka Power Log"));

  lcdBegin();
  setContrast(55);
  lcdPrint(VER, 2, 1);
  lcdPrint("   SINGLE  ", 3, 1);
  lcdPrint("   SMART   ", 4, 1);
  lcdPrint(" SOLUTIONS ", 5, 1);
  attachInterrupt(micGate, mic, FALLING);

  sendingDataSet.DEVICE_STATE = MASTER_STATE;

  DateTime (localRTC);

  errorCounterSync(); // Sync EEROR Counter by read SD card
  Serial.print(F("Last errorCounter "));
  Serial.println(errorCounter);

#ifdef __GSM__
  for (int zz = 0; zz < 100; zz++)AT_STATE[zz] = -1;
#endif


  config7758();// configure int regs of AD7758

  readEnergyInit();

  char tempLCD[20];
  String tempStr = "CRN:";
  tempStr += (String)sendingDataSet.DEVICE_SN;
  tempStr.toCharArray(tempLCD, tempStr.length() + 1);
  lcdPrint(tempLCD, 1, BLACK);

  Serial.println(sendingDataSet.SD_COUNTER);
  SD_LAST_5_DATA();

#ifdef __GSM__
  sendRemainData();
  gsmPowerOn();
  msgPrint("GSMconnecting");
  gsmSetup();
  msgPrint("IP connecting");
  gprsSetup();
  sdErrorLog(11);
  uint8_t statusReg = EEPROM.read(MCUSR_EEPROM_LOC);// read status register
  sdErrorLog(statusReg | 0x80); //  OR with B10000000


  /*##############################################################################################*/
  String sms_text = (String)sendingDataSet.DEVICE_SN;
  sms_text += " ePro1000 ";
  //add string for startup msg with reason of restart
  for (char rc = 0; rc < 4; rc++) {
    if (bitRead(statusReg, rc)) {
      sms_text += RESTART_CLAUSES[rc];
      sms_text += ",";
    }
  }
  if (statusReg == 0) {// when watch dog occur MCUSR register is become zero
    sms_text += RESTART_CLAUSES[3];
  }
  sms_text += '\n';
  sms_text += (String)VER;

  sendSMS(sms_text, configPhoneNum2);
  sendSMS(sms_text, configPhoneNum1);
  sendingDataSet.DEVICE_STATE = statusReg | 0x80;
  if (statusReg == 5) {
    sms_text = (String)sendingDataSet.DEVICE_SN;
    sms_text += "Dear Customer Your ePRO1000 POWER ON\n";
    sendSMS(sms_text, configPhoneNum3);

  }
  delay(5000);
  /*##############################################################################################*/
#endif

  setupInt();
  wdt_enable(WDTO_8S);

}

void loop() {
  wdt_reset();
  //  delay(9200);
  time = micros();

  if (((time - inte100ms) / 1000) > 100) {
    inte100ms = time;
    intRead7758();
    //    Serial.println(freeMemory());
  }

  if (((time - inte500ms) / 1000) > 500) {
    inte500ms = time;
    displayOn(micCount);
  }

  if (writeDataSDFlag) {
    writeDataSDDo();
    energyEEWrite();
    writeDataSDFlag = false;
  }

#ifdef __GSM__
  stateMachine();
#endif


  if ((time - inte15Sec) / 1000 > (oneMinute / 4)) {
    inte15Sec = time;
    if (lcdBackLight) {
      lcdBackLight = false;
      digitalWrite(blPin, HIGH);
      micCount = 0;
      displayOn(1);
    }
  }

}


void mic() {
  if (((time - inte15Sec) > 250) && (micCount == micTemp)) {
    inte15Sec = time;
    digitalWrite(blPin, LOW);
    micCount++;
    lcdBackLight = true;
  }
  if (micCount >= 5)micCount = 0;

}

void errorPrint(char error[])
{
  Serial.println(error);
  lcdPrint(error, 3, 1);
}


void msgPrint(char error[])
{
  Serial.println(error);
  lcdPrint(error, 6, 1);
}

void writeBinaryDATA() {
  CALC_CHECKSUM();
  byte DATA_BUFFER[sizeof(sendingDataSet)];
  for (byte ADD = 0; ADD < sizeof(sendingDataSet); ADD++) {
    DATA_BUFFER[ADD] = *((byte *)&sendingDataSet + ADD);
  }


  if (SDCARDclass_writeblock(SD_CLUSTER_START + sendingDataSet.SD_COUNTER, DATA_BUFFER) == 0) {
    sendingDataSet.SD_COUNTER++;
    energyEEWrite();
    for (int i = 0; i < sizeof(sendingDataSet); i++)
      Serial.print((byte)DATA_BUFFER[i]);
  } else
    Serial.println("ERROR WRITE SD");
}


void writeDataSDDo() {

  byte  DATA_BUFFER[512];


  String outString = ""; // had to use it sprintf doesnot alow long int

  averageSec();
  outString += String(sendingDataSet.SD_COUNTER);
  outString += ",";
  outString += String(localRTC);
  outString += ",";
  outString += String(sendingDataSet.DEVICE_SN);
  outString += ",";
  for (byte pp = 0; pp < 28 ; pp++) {
    outString += String(sendingDataSet.PARAMETERS[pp]);
    outString += ",";
  }
  outString += String(sendingDataSet.DEVICE_STATE);;
  outString += ",";
  outString += String(sendingDataSet.SIGNAL_STRENGTH);
  outString += ",";


  //  sendDataString += outString;

  outString += String(aReactiveEnergy, HEX);
  outString += ",";
  outString += String(bReactiveEnergy, HEX);
  outString += ",";
  outString += String(cReactiveEnergy, HEX);
  outString += ",";
  outString += String(aActiveEnergy, HEX);
  outString += ",";
  outString += String(bActiveEnergy, HEX);
  outString += ",";
  outString += String(cActiveEnergy, HEX);
  outString += ",";
  outString += String(aApparentEnergy, HEX);
  outString += ",";
  outString += String(bApparentEnergy, HEX);
  outString += ",";
  outString += String(cApparentEnergy, HEX);
  outString += ",";
  outString += (char)(DECRYPTOR);

  while (outString.length() < 510) {
    outString += (char)(32);
  }


  outString.getBytes(DATA_BUFFER, 512);
  int8_t hardTry = 10; // nums of tryies for write on SD card
  do {
    if (SDCARDclass_writeblock(uint32_t(DEVICE_SD_START[0] + (sendingDataSet.SD_COUNTER % 262144)), DATA_BUFFER) == 0) {
      sendingDataSet.SD_COUNTER++;
      energyEEWrite();
      for (int i = 0; i < 512; i++)
        Serial.print((char)DATA_BUFFER[i]);
      hardTry = 0;
      break;
    } else {
      hardTry--;
      Serial.println("ERROR WRITE SD");
      if(hardTry == 0){ // after 10 times neglect this particular SD cluster and move to next one   
        Serial.print("SD CLUSTER CRASHED SD COUNTER INCRE: ");
        Serial.println(sendingDataSet.SD_COUNTER);
        sendingDataSet.SD_COUNTER++;
        break;
      }
    }
  } while (hardTry > 0);


  sendingDataSet.SIGNAL_STRENGTH = 0; // if signal is available this varibale will update
  sendingDataSet.DEVICE_STATE = MASTER_STATE; //
}

void SD_LAST_5_DATA() {
  String sendDataStr = "";
  byte sendDataByte[512];
  for (uint8_t y = SENDING_DATA_PERIOD; y > 0; y--) {
    sendDataStr = "";
    if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[0] + (sendingDataSet.SD_COUNTER % 262144)) - y, sendDataByte) == 0) {
      for (int con = 0; con < 512; con++) {
        char tempChar = (char)sendDataByte[con];
        if (tempChar != DECRYPTOR) {
          Serial3.write(sendDataByte[con]);
          Serial.write(sendDataByte[con]);
        } else {
          Serial3.println();
          Serial.println();
          break;
        }
      }
    }
  }
  sendDataStr = "";
}


void CALC_CHECKSUM() {
  byte XOR = 0;
  byte STRUCT_ADD[sizeof(sendingDataSet)];
  for (byte ADD = 0; ADD < sizeof(sendingDataSet) - 1; ADD++) {
    STRUCT_ADD[ADD] = *((byte *)&sendingDataSet + ADD);
    XOR ^= STRUCT_ADD[ADD];
  }
  sendingDataSet.CSB = XOR;
}

#include <MemoryFree.h>

#include <EEPROM.h>

#include <TimerOne.h>

#include <avr/pgmspace.h>

#include <SPI.h>

#include <avr/wdt.h>


const uint32_t DEVICE_SD_START[] = {524288, 786432, 1048576, 1310720, 1572864, 1835008, 2097152, 2359296, 2621440, 2883584};
const uint32_t ERROR_COUNTER_SATRT = 262144;

#define MAX_EEPROM_ENDURANCE 78
#define MAX_GSM_ERROR_THRESHOLD 50
#define K_EEPROM_START 4000 // End with after 96 bytes 
#define CRN_EEPROM_START 3926 // End with after 4 bytes
#define OFF_SET_EEPROM_START 3970 // End with after 30 bytes 
#define DEVICE_IDS_EEPROM_START 3926//End with after 24 bytes 
#define TCP_SEND_BUFF_SIZE 1024
#define TCP_SEND_SIGNAL_STRENGTH 8
#define oneMinute 60000
#define TCP_MSG_SIZE  400
#define MASTER_STATE 0
#define SENDING_DATA_PERIOD 5

#define CURRENT_THRESHOLD 1000

#define ENCRYPTOR 110 //n

//#define DECRYPTOR ENCRYPTOR^10
#define DECRYPTOR 10


#define SIM900

#define SERIAL_RX_BUFFER_SIZE 256

float K[8][3] = {0.0};

char VER[] = "V0.9.4.5";
//char IP[] = "123.231.13.134"; // have to be in inv commas
//char IP[] = "123.231.15.99";
//char IP[] = "development.enetlk.com"; // now can use with google DNS
char IP[] = "db9.enetlk.com"; // now can use with google DNS
char port[] = "5001";//5200
//char port[] = "2500";
char APN[] = "mobitel";
String configPhoneNum1 = "+94710795484";
String configPhoneNum2 = "+94714758559";// Don't change this number Jlanka HQ
//String configPhoneNum2 = "+94718928585";
//char configPhoneNum[] = "+94712281322";

int fiveMinCounter = 0, restartCounter = 0;
byte gsmErrorCounter = 0, tempGSM = 0, ipConnectingCounter, deviceNeedRestart = 0;
String myTime = "1111/11/11 11:11:11";
unsigned long time = 0, inte15Sec = 0, oldTime = 0, inte500ms = 0, inte100ms = 0;
byte micTemp = 0, micCount = 0;
boolean lcdBackLight = true;







String Serail2RX = "";
String GENARAL_RES = "";
uint32_t ATtimer[3], errorCounter, localRTC = 0;
boolean ATFlag = false, responceFlag = false, writeDataSDFlag = false, sendDataFlag = false, revSMSFlag = false, readSMSFlag = false;
byte AT_RESPONCE_FLAG[3];
byte myMin = 0, mySec = 0;
String AT_RESPONCE[3];
boolean TCP_CONNECT_FLAG = false, CONNECTION_OPEN = false;
char AT_STATE[100];
int8_t AT_STATE_ATTEMPT[100];
byte AT_STATE_DUPLICATE = -1;
String smsStore;
String phoneNumber = "";
byte SMSposition = 0, SMS_STORE_COUNTER = 0;;

uint32_t DEVICE_IDS[6];
uint32_t DEVICE_SD_COUNTERS[6];
boolean transmitter = false;




void setupInt(void) {
  Timer1.initialize(5053302);// max period 8388608 us (8.3... sec)
  Timer1.attachInterrupt(myIsr); // isr run every 5 seconds
}

void myIsr(void) {
  mySec++;
  localRTC += 5;
  if (mySec >= 12) {
    myMin++;
    restartCounter++;
    writeDataSD(); // flag
    mySec = 0;
  }
  if (myMin >= SENDING_DATA_PERIOD) {
    sendTCPData();
    myMin = 0;
  }
}

void writeDataSD() {
  writeDataSDFlag = true;
}

void sendTCPData() {
  for (int x = 3; x < 50; x++)AT_STATE[x] = -1;// have to Recheck assume MODEM IS IDEAL
  sendDataFlag = true;
  ipConnectingCounter++;
}

#include "pin.h"
#include "SDCARD.h"
#include "mpa7758.h"
#include "states.h"
#include "information.h"
#include "mpaGSM_prog.h"
#include "mpagprs.h"
#include "mpaNokia5110.h"
#include "mpaEEPROM.h"
#include "mpaSMSprog.h"
#include "displayData.h"
#include "mpaTime.h"
#include "slaveControl.h"


void errorPrint();
void msgPrint();


void setup() {
  pinSetup();



  SPI.begin();



  SPI.setDataMode(SPI_MODE0);// for SD and LCD
  SPI.setClockDivider(SPI_CLOCK_DIV4);
  SPI.setBitOrder(MSBFIRST);
  Serial.begin(115200);
  Serial3.begin(9600);
  //  Serial2.begin(57600);


  Serial.println("Starting the Prog");
  Serial.println(VER);
  Serial.println("Jlanka Power Log");


  lcdBegin();
  setContrast(55);
  lcdPrint(VER, 2, 1);
  lcdPrint("   SINGLE   ", 3, 1);
  lcdPrint("   SMART   ", 4, 1);
  lcdPrint(" SOLUTIONS ", 5, 1);
  attachInterrupt(micGate, mic, FALLING);

  sendingDataSet.DEVICE_STATE = MASTER_STATE;

  errorCounterSync(); // Sync EEROR Counter by read SD card
  Serial.print(F("Last errorCounter "));
  Serial.println(errorCounter);

  //  //      EEPROM_SD_COUNTER_WRITE(0);
  for (int zz = 0; zz < 100; zz++)AT_STATE[zz] = -1;

  config7758();// configure int regs of AD7758

  readEnergyInit();

  //  DEVICE_SD_COUNTERS[0] = 0;
  //  energyEEWrite();
  //  Serial.println("test");
  //  delay(5000);
  //  DEVICE_IDS[1] = 10001;
  //  deviceIdWrite();

  char tempLCD[20];
  String tempStr = "CRN:";
  tempStr += (String)DEVICE_IDS[0];
  tempStr.toCharArray(tempLCD, tempStr.length() + 1);
  lcdPrint(tempLCD, 1, BLACK);

  Serial.println(DEVICE_SD_COUNTERS[0]);

  sendDataGPRSDo(0, SENDING_DATA_PERIOD);
  sendingDataSet.TIME_STAMP = 946684800;

  gsmPowerOn();
  msgPrint("GSMconnecting");
  gsmSetup();
  msgPrint("IP connecting");
  gprsSetup();
  sdErrorLog(11);
  uint8_t statusReg = MCUSR;// read status register
  sdErrorLog(statusReg | 0x80); //  OR with B10000000
  Serial.print("Reason Of MCU Restart: ");
  Serial.println(statusReg);
  char sms_text[160];
  sprintf(sms_text, "(%ld) ePro1000 Power ON\n%s", DEVICE_IDS[0], VER);
  sendSMS((String)sms_text, configPhoneNum2);
  sendSMS((String)sms_text, configPhoneNum1);

  delay(5000);
  setupInt();
  wdt_enable(WDTO_8S);

}

void loop() {
  wdt_reset();
  //  Serial.println(freeMemory());

  time = micros();
  if (((time - inte100ms) / 1000) > 100) {
    //    read7758();
    intRead7758();
    inte100ms = time;
  }


  if (((time - inte500ms) / 1000) > 500) {
    inte500ms = time;
    displayOn(micCount);
    if (CONNECTION_OPEN) {
      msgPrint("Sending Data..");
    }
    if ((sendDataFlag) && (!CONNECTION_OPEN)) {
      msgPrint("IP connecting");
    }
  }

  if ((gsmErrorCounter >= 50) || (ipConnectingCounter >= 10)) {
    gsmErrorCounter = 0;
    ipConnectingCounter = 0;
    gsmPowerOn();
    msgPrint("GSMconnecting");
    gsmSetup();
    msgPrint("IP connecting");
    gprsSetup();
    restartFlagDisable();
    deviceNeedRestart++;
  }
  if (( deviceNeedRestart > 5)  || (restartCounter >= 180)){
    wdt_enable(WDTO_1S);
    delay(5000);
  }
  if ((writeDataSDFlag)) {
    averageSec();
    writeBinaryDATA(0);
    energyEEWrite();
    writeDataSDFlag = false;
  }

  //  if (ATCondition(1)) {
  //    sendAT("AT", 0, 1, 500, 100, 0);
  //  }

  if ((AT_STATE_READER(21)) && (sendDataFlag)) {
    TCP_CONNECT_FLAG = true;
    sendDataFlag = false;
    CONNECTION_OPEN = true;
    for (int x = 3; x < 50; x++)AT_STATE[x] = -1;
  } else if ((AT_STATE[21] == -4) || (AT_STATE[21] == -2)) {
    if(AT_STATE[21] == -4){
      gsmErrorCounter = 60;
    }
    sendDataFlag = false;
    for (int x = 3; x < 50; x++)AT_STATE[x] = -1;
    TCP_CLOSE();
  }

  if ((sendDataFlag) && (!CONNECTION_OPEN)) { // IP Connecting Function
    gprsSendSetup();
  }


  if (TCP_CONNECT_FLAG) {
    sendDataGPRSDo(0, SENDING_DATA_PERIOD);
    //    if (ATCondition(26)) {
    Serial.println("Ready for read from server");
    sendATx( 22, 26, 15000, 200, 1);
    //    }

    TCP_CONNECT_FLAG = false;
  }

  if (((AT_STATE[26] == -2) || (AT_STATE[26] == -4)) && (CONNECTION_OPEN)) {
    TCP_CLOSE();
    if (AT_STATE[27] <= -3) {
      Serial.println("CONNECTION CLOSED");
      AT_STATE[26] = -1;
      CONNECTION_OPEN = false;
    }

  }

  if (ATCondition(1)) {
    sendAT("AT +CSQ\r\n", 19, 1, 500, 200, 1, 2);
  }


  if (ATCondition(2)) {
    sendAT("AT +CCLK?\r\n", 18, 2, 500, 200, 1, 2);
  }

  if (ATCondition(50)) {
    if (SMS_STORE_COUNTER > 24) SMS_STORE_COUNTER = 0;
    char buff[20];
    SMS_STORE_COUNTER++;
    sprintf(buff, "AT+CMGR = %d\r\n", SMS_STORE_COUNTER);
    sendAT(buff, 0, 50, 1500, 100, 1, 1);
  }

  if (resCondition(1)) {
    sendingDataSet.SIGNAL_STRENGTH = signalStrength();
    Serial.println(sendingDataSet.SIGNAL_STRENGTH );
    AT_STATE[1] = -1;
  }

  if (resCondition(2)) {
    getDateTime();
    AT_STATE[2] = -1;
  }

  if (resCondition(50)) {
    if ((ATCondition(51)) && (AT_STATE_READER(50))) {
      if (smsStore.length() > 0) {
        char msgResTemp = msgOperation();
        char buff[20];
        sprintf(buff, "AT+CMGD = %d,0\r\n", SMSposition);
        sendAT(buff, 0, 51, 500, 100, 1, 2);
        if ((msgResTemp == 3) || (msgResTemp == 4)) {
          if (msgResTemp == 3) {
            errorLog(100, 51);
          }
          delay(5000);
        }
        SMSposition = 0;
        smsStore = "";
      }
      AT_STATE[50] = -1;
    } else {
      AT_STATE[50] = -1;
      AT_STATE[51] = -1;
    }
    if (AT_STATE[2] == -4)AT_STATE[2] = -1;
    if (AT_STATE[1] == -4){
      AT_STATE[1] = -1;
      sendingDataSet.SIGNAL_STRENGTH = 0xFF;
    }
  } else if (AT_STATE[50] == -4) {
    if (AT_STATE[2] == -4)AT_STATE[2] = -1;
    if (AT_STATE[1] == -4){
      AT_STATE[1] = -1;
      sendingDataSet.SIGNAL_STRENGTH = 0xFF;
    }
    AT_STATE[50] = -1;
  }

  if (resCondition(26)) {
    sendRemainData();
    ipConnectingCounter = 0;
    deviceNeedRestart = 0;
    AT_STATE[26] = -4;
  }

  if (ATFlag) {
    sim900Rx();
  }

  Serial2Read();

  if ((time - inte15Sec) / 1000 > (oneMinute / 4)) {
    inte15Sec = time;
    if (lcdBackLight) {
      lcdBackLight = false;
      digitalWrite(blPin, HIGH);
      micCount = 0;
      displayOn(1);
    }
  }

}

void restartFlagDisable() {
  readSMSFlag = false;
  CONNECTION_OPEN = false;
  TCP_CONNECT_FLAG = false;
  sendDataFlag = false;
  CONNECTION_OPEN = false;
  ATFlag = false;
  for (int x = 0; x < 100; x++)AT_STATE[x] = -1;
}
void mic() {
  if (((time - inte15Sec) > 250) && (micCount == micTemp)) {
    inte15Sec = time;
    digitalWrite(blPin, LOW);
    micCount++;
    lcdBackLight = true;
  }
  if (micCount >= 5)micCount = 0;

}

void errorPrint(char error[])
{
  Serial.println(error);
  lcdPrint(error, 3, 1);
}


void msgPrint(char error[])
{
  Serial.println(error);
  lcdPrint(error, 6, 1);
}

void writeBinaryDATA(byte deviceState) {
  byte SD_DATA_BUFFER[512];
  sendingDataSet.SD_COUNTER = DEVICE_SD_COUNTERS[deviceState];
  sendingDataSet.DEVICE_SN = DEVICE_IDS[deviceState];
  sendingDataSet.TIME_STAMP += localRTC;
  CALC_CHECKSUM();
  if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[deviceState] + (DEVICE_SD_COUNTERS[deviceState]  / 4)), SD_DATA_BUFFER) == 0) {
    for (uint16_t ADD = ((DEVICE_SD_COUNTERS[deviceState] % 4) * 128); ADD < ((DEVICE_SD_COUNTERS[deviceState] % 4) * 128) + sizeof(sendingDataSet); ADD++) {
      byte sdtemp =  *((byte *)&sendingDataSet + (ADD % 128));
      SD_DATA_BUFFER[ADD] = sdtemp;
      Serial.print(sdtemp);
      Serial.print(" ");
    }
    Serial.println();
    if (SDCARDclass_writeblock(uint32_t(DEVICE_SD_START[deviceState] + (DEVICE_SD_COUNTERS[deviceState] / 4)), SD_DATA_BUFFER) == 0) {
      DEVICE_SD_COUNTERS[deviceState]++;
      energyEEWrite();
    } else {
      Serial.println("ERROR WRITE SD");
      DEVICE_SD_COUNTERS[deviceState]++;
      energyEEWrite();
    }
  }
}

void sendDataGPRSDo(byte deviceState, byte numberOfData) {
  uint32_t tempPos = 0;
  byte SD_DATA_BUFFER[512];
  for (int8_t y = numberOfData; y > 0; y--) {
    if ((((DEVICE_SD_COUNTERS[deviceState] - y) / 4) == tempPos) && (DEVICE_SD_COUNTERS[deviceState] != 0)) { // if before read cluster is same no need to read it again therfore use this if condition
      union {
        float f;
        unsigned long a;
        byte b[4];
      } bconvf;
      Serial3.flush();
      for (uint16_t con = (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128); con < (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128) + 128; con++) {
        Serial3.write(SD_DATA_BUFFER[con]);
        Serial.print(SD_DATA_BUFFER[con]);
        Serial.print(' ');
        bconvf.b[con % 4] = SD_DATA_BUFFER[con];
        if ((con % 4) == 3) {
          if ((con % 128) < 3 * 4) {
            Serial.print(bconvf.a);
            Serial.print(',');
          } else {
            Serial.print(bconvf.f);
            Serial.print(',');
          }
        }
      }
      Serial.println();
    } else {
      union {
        float f;
        unsigned long a;
        byte b[4];
      } bconvf;
      if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[deviceState] + ((DEVICE_SD_COUNTERS[deviceState] - y) / 4)), SD_DATA_BUFFER) == 0) {
        Serial3.flush();
        for (uint16_t con = (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128); con < (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128) + 128; con++) {
          Serial3.write(SD_DATA_BUFFER[con]);
          Serial.print(SD_DATA_BUFFER[con]);
          Serial.print(' ');
          bconvf.b[con % 4] = SD_DATA_BUFFER[con];
          if ((con % 4) == 3) {
            if ((con % 128) < 3 * 4) {
              Serial.print(bconvf.a);
              Serial.print(',');
            } else {
              Serial.print(bconvf.f);
              Serial.print(',');
            }
          }
        }
        Serial.println();
        //        for (int con = 0; con < sizeof(SD_DATA_BUFFER); con++) {
        //          Serial.print((int8_t)SD_DATA_BUFFER[con]);
        //          Serial.print(' ');
        //        }
      }
      tempPos = ((DEVICE_SD_COUNTERS[deviceState] - y) / 4);
    }
  }
}

char msgOperation() {
  char ret = 0;
  String sms = (String)smsStore;
  int comma[10];
  unsigned long CRN;


  if ((phoneNumber.indexOf(configPhoneNum1) >= 0) || (phoneNumber.indexOf(configPhoneNum2) >= 0)) {
    ret = 1;
    Serial.println("SMS Operation OK");
    if (sms.startsWith("SET CRN")) {
      ret = 2;
      DEVICE_IDS[0] = sms.substring(sms.indexOf(':') + 1).toInt();
      deviceIdWrite();
      sendingDataSet.DEVICE_SN = CRN;
    } else if (sms.startsWith("RESTART")) {
      ret = 3;
      wdt_enable(WDTO_4S);
    } else if (sms.startsWith("RESET CRN")) {
      ret = 4;
      aActiveEnergy = 0;
      bActiveEnergy = 0;
      cActiveEnergy = 0;
      aReactiveEnergy = 0;
      bReactiveEnergy = 0;
      cReactiveEnergy = 0;
      aApparentEnergy = 0;
      bApparentEnergy = 0;
      cApparentEnergy = 0;
      DEVICE_IDS[0] = sms.substring(sms.indexOf(':') + 1).toInt();
      deviceIdWrite();
      sendingDataSet.DEVICE_SN = CRN;
      OFFSET_CONFIG();

      energyEEWrite();
      wdt_enable(WDTO_4S);
      //      wdt_enable(WDTO_2S);
    } else if (sms.startsWith("RED")) {
      ret = 5;
      for (byte x = 0; x < 9; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
    else if (sms.startsWith("YELLOW")) {
      ret = 6;
      for (byte x = 0; x < 8; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
    else if (sms.startsWith("BLUE")) {
      ret = 7;
      for (byte x = 0; x < 8; x++) {
        if (x == 0)comma[0] = sms.indexOf(',');
        else comma[x] = sms.indexOf(',', comma[x - 1] + 1);
      }
      for (byte x = 0; x < 9; x++) {
        K[x][0] = sms.substring(comma[x] + 1, comma[x + 1]).toFloat();
      }
      EEPROM_K_WRITE();
    }
  }
  return ret;
}


void CALC_CHECKSUM() {
  byte XOR = 0;
  byte STRUCT_ADD[sizeof(sendingDataSet)];
  //  byte* STRUCT_ADD = new byte[sizeof(sendingDataSet)];
  Serial.print("Going to check CSB ");
  for (byte ADD = 0; ADD < sizeof(sendingDataSet) - 2; ADD++) {
    STRUCT_ADD[ADD] = *((byte *)&sendingDataSet + ADD);
    Serial.print(STRUCT_ADD[ADD]);
    Serial.print(" ");
    XOR ^= STRUCT_ADD[ADD];
  }
  Serial.println();
  sendingDataSet.CSB = XOR;
}

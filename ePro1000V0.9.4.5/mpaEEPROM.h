
// EEPROM Structure
//byte[3] = EEPROMConter - 0
//byte[4] = aActiveEnergy - 3
//byte[4] = bActiveEnergy - 7
//byte[4] = bActiveEnergy - 11
//byte[4] = aReactiveEnergy - 15
//byte[4] = bReactiveEnergy - 19
//byte[4] = cReactiveEnergy - 23
//byte[4] = aApparentEnergy - 27
//byte[4] = bApparentEnergy - 31
//byte[4] = cApparentEnergy - 35
//byte[3] = masterSDCounter - 39
//finish - 42


//unsigned long count = 0;// use to store the no of times EEprom address writen
//unsigned int eepromStart = 0;// energy data storerage start address
unsigned long eepromCnt = 0;

int readConf();
//unsigned long EEPROM_CRN_READ();



void energyEERead() {
  uint32_t a[3][3];// used to store energy data
  union {
    unsigned long a;
    byte b[4];
  } energyRead;
  Serial.println("**********Energy DATA**************");
  for (int i = 0; i < 3; i++) { // write energy data to eeprom
    for (int j = 0; j < 3; j++) {
      for (int k = 0; k < 4; k++) {
        energyRead.b[k] =  EEPROM.read((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42 + 3 + k + ((i * 3 + j) * 4));
      }
      a[i][j] = energyRead.a;
      Serial.print(a[i][j]);
      Serial.print(',');
    }
    Serial.println();
  }

  for (char k = 0; k < 3; k++) {
    energyRead.b[k] = EEPROM.read((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42 + 39 + k);
  }
  energyRead.b[3] = 0;
  DEVICE_SD_COUNTERS[0] = energyRead.a;
  
  

  aActiveEnergy = a[0][0];
  bActiveEnergy = a[0][1];
  cActiveEnergy = a[0][2];
  aReactiveEnergy = a[1][0];
  bReactiveEnergy = a[1][1];
  cReactiveEnergy = a[1][2];
  aApparentEnergy = a[2][0];
  bApparentEnergy = a[2][1];
  cApparentEnergy = a[2][2];
}

void deviceIdInit() {
  union {
    unsigned long a;
    byte b[4];
  } intslvid;
  for (byte slv = 0; slv < 6; slv++) {
    for (byte slvid = 0; slvid < 4; slvid++) {
      intslvid.b[slvid] = EEPROM.read(DEVICE_IDS_EEPROM_START + (slv * 4) + slvid);
    }
    DEVICE_IDS[slv] = intslvid.a;
  }
}

void deviceIdWrite() {
  union {
    unsigned long a;
    byte b[4];
  } wrtslvid;
  for (byte slv = 0; slv < 6; slv++) {
    wrtslvid.a = DEVICE_IDS[slv];
    for (byte slvid = 0; slvid < 4; slvid++) {
      EEPROM.write(DEVICE_IDS_EEPROM_START + (slv * 4) + slvid, wrtslvid.b[slvid]);
    }
  }
}


void readEnergyInit() {
  uint32_t oldCnt = 0, newCnt = 0;
  uint16_t eepromFind = 0;
  readConf();
  deviceIdInit();
  do {
    union {
      unsigned long a;
      byte b[4];
    } readcrn;
    readcrn.b[3] = 0;
    for (int8_t k = 2; k >= 0; k--) {
      readcrn.b[k] = EEPROM.read(eepromFind + k);
    }
    oldCnt = readcrn.a;
    readcrn.b[3] = 0;
    for (int8_t k = 2; k >= 0; k--) {
      readcrn.b[k] = EEPROM.read(eepromFind + 42 + k);
    }
    newCnt = readcrn.a;

    if ((oldCnt + 1) == newCnt) {
      eepromFind += 42;
    } else if (oldCnt == newCnt) {
      eepromCnt = 0;
    } else {
      eepromCnt = oldCnt;
    }

    if (eepromFind >= (MAX_EEPROM_ENDURANCE * 42)) {
      eepromCnt = newCnt;
      break;
    }
  } while ((oldCnt + 1) == newCnt);

  energyEERead();

}


void energyEEWrite() {
  uint32_t a[3][3];// used to store energy data
  eepromCnt++;
  noInterrupts();
  union {
    unsigned long a;
    byte b[4];
  } savecrn;
  savecrn.a = eepromCnt;
  for (int j = 0; j < 3; j++) {
    EEPROM.write((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42 + j, savecrn.b[j]);
  }
  a[0][0] = aActiveEnergy;
  a[0][1] = bActiveEnergy;
  a[0][2] = cActiveEnergy;
  a[1][0] = aReactiveEnergy;
  a[1][1] = bReactiveEnergy;
  a[1][2] = cReactiveEnergy;
  a[2][0] = aApparentEnergy;
  a[2][1] = bApparentEnergy;
  a[2][2] = cApparentEnergy;
  for (int i = 0; i < 3; i++) { // write energy data to eeprom
    for (int j = 0; j < 3; j++) {
      savecrn.a = a[i][j];
      for (int k = 0; k < 4; k++) {
        EEPROM.write((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42 + 3 + k + ((i * 3 + j) * 4), savecrn.b[k]);
      }
    }
  }
  savecrn.a = DEVICE_SD_COUNTERS[0];
  for (char k = 0; k < 3; k++) {
    EEPROM.write((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42 + 39 + k, savecrn.b[k]);
  }

  interrupts();
  Serial.print("Counter ");
  Serial.println(eepromCnt);
  Serial.print("EEPROM POS ");
  Serial.println((eepromCnt % (MAX_EEPROM_ENDURANCE + 1)) * 42);


}



void EEPROM_K_WRITE() {
  for (byte i = 0; i < 8; i++) {
    for (byte j = 0; j < 3; j++) {
      union {
        float a;
        byte b[4];
      } fconvb;
      fconvb.a = K[i][j];
      for (int x = K_EEPROM_START + (i * 3 + j) * 4, y = 0; x < K_EEPROM_START + (i * 3 + j) * 4 + 4, y < 4; x++, y++) {
        EEPROM.write(x, fconvb.b[y]);
        Serial.print(x);
        Serial.print(",");
      }
      Serial.print(K[i][j]);
      Serial.println();
    }
  }

}



void EEPROM_K_READ() {
  for (byte i = 0; i < 8; i++) {
    for (byte j = 0; j < 3; j++) {
      union {
        float a;
        byte b[4];
      } bconvf;
      for (int x = K_EEPROM_START + (i * 3 + j) * 4, y = 0; x < K_EEPROM_START + (i * 3 + j) * 4 + 4, y < 4; x++, y++) {
        bconvf.b[y] = EEPROM.read(x);
      }
      K[i][j] = bconvf.a;
      Serial.print(K[i][j]);
      Serial.print(",");
    }
    Serial.println();
  }
}


int readConf()
{
  boolean eepromFlag = false;

  Serial.println("K read 1");
  EEPROM_K_READ();
  Serial.println("K read 2");

  for (int x = 0; x < 8; x++) {
    for (int y = 0; y < 3; y++) {
      if ((K[x][y] > 0) && (K[x][y] != 0xFFFFFFFF))
        eepromFlag = 1;
    }
  }
  if (eepromFlag == 0) {

  } else EEPROM_K_READ();
}


void errorCounterSync() {
  byte SD_DATA_BUFFER[512];
  union {
    unsigned long a;
    byte b[4];
  } bconvf;
  if (SDCARDclass_readblock(ERROR_COUNTER_SATRT, SD_DATA_BUFFER) == 0) { //need to find out factor
    byte factor;
    uint32_t low = 0, high = 131071, mid = 0;
    for (byte i = 0; i < 3; i++) {
      bconvf.b[i] = SD_DATA_BUFFER[i];
    }
    bconvf.b[3] = 0;
    factor = bconvf.a / 131072;

    for (byte i = 0; i < 17; i++) {  // (64UL * mid * ((uint32_t)factor + 1)) == bconvf.a
#ifdef DEBUG_SD
      Serial.print("HIGH\t");
      Serial.print(high);
      Serial.print("\tLOW\t");
      Serial.print(low);
#endif
      mid = ((low + high) + 1) / 2;
#ifdef DEBUG_SD
      Serial.print("\tMID\t");
      Serial.print(mid);
#endif
      if (SDCARDclass_readblock(ERROR_COUNTER_SATRT + mid, SD_DATA_BUFFER) == 0) {
        for (byte k = 0; k < 3; k++) {
          bconvf.b[k] = SD_DATA_BUFFER[k];
        }
        bconvf.b[3] = 0;
#ifdef DEBUG_SD
        Serial.print("\t64UL\t");
        Serial.print(64UL * mid * ((uint32_t)factor + 1));
        Serial.print("\t1st Val\t");
        Serial.println(bconvf.a);
#endif
        if (i == 16) { // after 17 times last counter can find
          if ((64UL * mid * ((uint32_t)factor + 1)) == bconvf.a) {// last cluster check which is last counter
            if (SDCARDclass_readblock(ERROR_COUNTER_SATRT + mid, SD_DATA_BUFFER) == 0) {// again read selected cluster
              int32_t clusterFirstVal = 0;
              for (byte k = 0; k < 3; k++) {
                bconvf.b[k] = SD_DATA_BUFFER[k];
              }
              bconvf.b[3] = 0;
              clusterFirstVal = bconvf.a;
#ifdef DEBUG_SD
              Serial.print("\tclusterFirstVal\t");
              Serial.println(clusterFirstVal);
#endif
              factor = bconvf.a / 131072;
              low = 0;
              high = 63;
#ifdef DEBUG_SD
              Serial.println("*******************************************************************************************");
#endif
              for (byte j = 0; j < 6; j++) {
#ifdef DEBUG_SD
                Serial.print("HIGH\t");
                Serial.print(high);
                Serial.print("\tLOW\t");
                Serial.print(low);
#endif
                mid = ((low + high) + 1) / 2;
#ifdef DEBUG_SD
                Serial.print("\tMID\t");
                Serial.print(mid);
#endif
                int32_t tempCalc;
                for (byte k = 0; k < 3; k++) {
                  bconvf.b[k] = SD_DATA_BUFFER[(mid * 8) + k];
                }
                bconvf.b[3] = 0;
                tempCalc = bconvf.a;
                tempCalc -= clusterFirstVal;
#ifdef DEBUG_SD
                Serial.print("\tbconvf.a\t");
                Serial.print(bconvf.a);
                Serial.print("\ttempCalc\t");
                Serial.println(tempCalc);
#endif
                if (j == 5 ) {
                  if (tempCalc == mid) {
                    for (byte k = 0; k < 3; k++) {
                      bconvf.b[k] = SD_DATA_BUFFER[(mid * 8) + k];
                    }
                    bconvf.b[3] = 0;
                    errorCounter = bconvf.a;
                    errorCounter++;
                  } else {
                    for (byte k = 0; k < 3; k++) {
                      bconvf.b[k] = SD_DATA_BUFFER[((mid - 1) * 8) + k];
                    }
                    bconvf.b[3] = 0;
                    errorCounter = bconvf.a;
                    errorCounter++;
                  }
                } else if (mid == tempCalc) {
                  low = mid + 1;
                } else {
                  high = mid - 1;
                }
              }
            }
          } else { // if not above cluster is contain last counter then current cluster - 1 should be contain last counter.
            if (SDCARDclass_readblock(ERROR_COUNTER_SATRT + mid - 1UL, SD_DATA_BUFFER) == 0) {
              int32_t clusterFirstVal = 0;
              for (byte k = 0; k < 3; k++) {
                bconvf.b[k] = SD_DATA_BUFFER[k];
              }
              bconvf.b[3] = 0;
              clusterFirstVal = bconvf.a;
              factor = bconvf.a / 131072;
              low = 0;
              high = 63;
#ifdef DEBUG_SD
              Serial.print("\t64UL\t");
              Serial.print(ERROR_COUNTER_SATRT + mid - 1UL);
              Serial.print("\t1st Val\t");
              Serial.println(bconvf.a);
              Serial.println("**********************************************************");
#endif

              for (byte j = 0; j < 6; j++) {
#ifdef DEBUG_SD
                Serial.print("HIGH\t");
                Serial.print(high);
                Serial.print("\tLOW\t");
                Serial.print(low);
#endif

                mid = ((low + high) + 1) / 2;
#ifdef DEBUG_SD
                Serial.print("\tMID\t");
                Serial.print(mid);
#endif
                int32_t tempCalc;
                for (byte k = 0; k < 3; k++) {
                  bconvf.b[k] = SD_DATA_BUFFER[(mid * 8) + k];
                }
                bconvf.b[3] = 0;
                tempCalc = bconvf.a;
                tempCalc -= clusterFirstVal;
#ifdef DEBUG_SD
                Serial.print("\t64UL\t");
                Serial.print((mid * 8));
                Serial.print("\ttempCalc\t");
                Serial.println(tempCalc);
#endif
                if (j == 5 ) {
                  if (tempCalc == mid) {
                    for (byte k = 0; k < 3; k++) {
                      bconvf.b[k] = SD_DATA_BUFFER[(mid * 8) + k];
                    }
                    bconvf.b[3] = 0;
                    errorCounter = bconvf.a;
                    errorCounter++;
                  } else {
                    for (byte k = 0; k < 3; k++) {
                      bconvf.b[k] = SD_DATA_BUFFER[((mid - 1) * 8) + k];
                    }
                    bconvf.b[3] = 0;
                    errorCounter = bconvf.a;
                    errorCounter++;
                  }
                } else if (mid == tempCalc) {
                  low = mid + 1;
                } else {
                  high = mid - 1;
                }
              }
            }
          }
          break;
        } else if ((64UL * mid * ((uint32_t)factor + 1)) == bconvf.a) {
          low = mid + 1;
        } else {
          high = mid - 1;
        }
      }
    }
  }
}


char sendATcmd(char* ATcommand, String expected_answer1, unsigned int timeout, char* responce2);
void gsmPowerOn();
char sendATcmd2(char* ATcommand, String expected_answer1, String unexpected_answer, unsigned int timeout, char* responce2);
char sendATcmd(char* ATcommand, String expected_answer1, unsigned int timeout);
void sim900Rx(void);
char sendAT2(char* ATcommand, char expecState, char state, char unExpecState, unsigned int timeout, unsigned int readStartTime, boolean res);
char sendAT(char* ATcommand, char expecState, char state, unsigned int timeout, unsigned int readStartTime, boolean res);
char sendRangeAT(char* ATcommand, char expecStateStart, char stateStart, char rangeSize, unsigned int timeout, unsigned int readStartTime, boolean res);
char AT_STATE_READER(byte state);
char resCondition(byte state);
char ATCondition(byte state);
void responceReader(byte state, char * resp);
uint8_t errorLog(int8_t state, int8_t answer);
void printError(byte errorByte);


byte st, ans;

char gprsSetup()
{
  char buff[100];// to put the command

  if (sendATcmd("AT", "OK", 500) != 1) {
    sdErrorLog(22);
  } else {
    // gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times
  // just to test AT mode is ok

  if (sendATcmd("AT+CREG=1", "OK", 500) != 1) {
    sdErrorLog(23);
  } else {
    // gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times
  // reg mode reply

  if (sendATcmd("AT+CIPMUX=0", "OK", 500) != 1) {
    sdErrorLog(24);
  } else {
    // gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times
  // IP single chanell


  sprintf(buff, "AT+CGDCONT=2,\"IP\",\"%s\"", APN);

  if (sendATcmd(buff, "OK", 500) != 1) {
    sdErrorLog(25);
  } else {
    // gsmErrorCounter  =  0;
  }
  // IP INIT to IP START
}

void gsmPowerOn()
{

  // first power OFF the device if ON
  if (digitalRead(GSM_STATUS) == LOW)
  {
    // GSM module is switched on now switch off
    Serial.println("Switching OFF the module");
    digitalWrite(GSM_ON, LOW); // make the pin low there is a inverting tr in gsm module to pin
    Serial.println("Pulse UP");
    delay(1000);
    wdt_reset();
    Serial.println("Pulse Down");
    digitalWrite(GSM_ON, HIGH); // make the pin high there is a inverting tr in gsm module to pin
    delay(3000);
    wdt_reset();
    Serial.println("Pulse UP wait 3 sec to Power OFF");
    digitalWrite(GSM_ON, LOW); // make the pin low there is a inverting tr in gsm module to pin
    delay(3000);
    wdt_reset();

  }

  Serial.println("Power should be OFF now");
  delay(1000);
  wdt_reset();
  if (digitalRead(GSM_STATUS) == HIGH)
  {
    // GSM module is switched off
    Serial.println("Powering on the GSM Module");
    digitalWrite(GSM_ON, LOW); // make the pin low there is a inverting tr in gsm module to pin
    delay(500);
    wdt_reset();
    digitalWrite(GSM_ON, HIGH); // make the pin high there is a inverting tr in gsm module to pin
    delay(1500);
    wdt_reset();
  }
  else
  {
    digitalWrite(SIM900_POWER, LOW);
    delay(1000);
    digitalWrite(SIM900_POWER, HIGH);
    // Initialy power shold be OFF ERROR!!
    Serial.println("ERROR in initial Power off");
    Serial.println("Again Powering on the GSM Module");
    digitalWrite(GSM_ON, LOW); // make the pin low there is a inverting tr in gsm module to pin
    delay(500);
    wdt_reset();
    digitalWrite(GSM_ON, HIGH); // make the pin high there is a inverting tr in gsm module to pin
    delay(1500);
    wdt_reset();
  }

  wdt_reset();
  digitalWrite(GSM_ON, LOW); // make the pin low there is a inverting tr in gsm module to pin
  delay(2500);

}

char sendATcmd(char* ATcommand, String expected_answer1, unsigned int timeout, char* responce2) {

  char answer = 0;
  char tempResponse;
  String responce = "";
  unsigned long previous;



  while ( Serial3.available() > 0) Serial3.read();
  wdt_reset();
  Serial3.println(ATcommand);
  Serial3.flush();
  wdt_reset();
  previous = millis();

  do {
    if (Serial3.available() > 0) {
      tempResponse = Serial3.read();
      if ((tempResponse == '\n') || (tempResponse == '\r') || (tempResponse == '>')) {
        //         Serial.println(responce);
        //         Serial.println(responce.lastIndexOf(expected_answer1));
        if (tempResponse == '>') responce += tempResponse;
        if (responce.indexOf(expected_answer1) >= 0) {
          responce.toCharArray(responce2, responce.length() + 1);
          answer = 1;
        }
        responce = "";
      } else {
        responce += tempResponse;
      }
    }
    wdt_reset();
  } while ((answer <= 0) && ((millis() - previous) < timeout));

  return answer;
}

char sendATcmd2(char* ATcommand, String expected_answer1, String unexpected_answer, unsigned int timeout, char* responce2) {

  char answer = 0;
  char tempResponse;
  String responce = "";
  unsigned long previous;




  while ( Serial3.available() > 0) Serial3.read();

  Serial3.println(ATcommand);
  Serial3.flush();

  previous = millis();

  do {
    if (Serial3.available() > 0) {
      tempResponse = Serial3.read();
      if (tempResponse == '\n') {
        //         Serial.println(responce);
        //         Serial.println(responce.lastIndexOf(expected_answer1));
        if (responce.indexOf(expected_answer1) >= 0) {
          responce.toCharArray(responce2, responce.length() + 1);
          answer = 1;
        } else if (responce.indexOf(unexpected_answer) >= 0) {
          answer = 2;
        }
        responce = "";
      } else {
        responce += tempResponse;
      }
    }
  } while ((answer <= 0) && ((millis() - previous) < timeout));

  return answer;
}

char sendATcmd(char* ATcommand, String expected_answer1, unsigned int timeout) {
  char temp[100];
  char ret;
  ret = sendATcmd(ATcommand, expected_answer1, timeout, temp);
  return ret;
}

void gsmSetup()
{
  if (sendATcmd("AT", "OK", 500) != 1) {
    sdErrorLog(22);
  } else {
    gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT Z", "OK", 500) != 1) {
    sdErrorLog(44);
  } else {
    gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT +IPR=9600", "OK", 500) != 1) {
    sdErrorLog(45);
  } else {
    gsmErrorCounter  =  0;
  }
  // Baud rate 9600


  if (sendATcmd("AT +CPIN?", "+CPIN: READY", 500) != 1) {
    sdErrorLog(46);
  } else {
    gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT +CLTS=1", "OK", 500) != 1) {
    sdErrorLog(47);
  } else {
    gsmErrorCounter  =  0;
  }
  // Local time stamp from network


  if (sendATcmd("AT +CREG=1", "OK", 500) != 1) {
    sdErrorLog(23);
  } else {
    gsmErrorCounter  =  0;
  }
  // Unsosalized Show the reg status


  for (int y = 0, x = 0; y < 30; y++) {
    x = sendATcmd("AT +CREG?", "OK", 1000);
    //    y++;
    //    if (x != 1)//             sdErrorLog(26);
    if (x == 1)y = 70;
  }// cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT +CMGF=1", "OK", 500) != 1) {
    sdErrorLog(48);
  } else {
    gsmErrorCounter  =  0;
  }
  // SET sms MODE TO TXT
  // cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT +CSQ", "OK", 500) != 1) {
    sdErrorLog(49);
  } else {
    gsmErrorCounter  =  0;
  }
  // cmd, res_del, int ch delay, exp res, no of times

  if (sendATcmd("AT +CNMI=1", "OK", 500) != 1) {
    sdErrorLog(50);
  } else {
    gsmErrorCounter  =  0;
  }
  // The way that SMS display // may have to change

  if (sendATcmd("AT +CPMS=\"SM\",\"SM\",\"SM\"", "OK", 500) != 1) {
    sdErrorLog(51);
  } else {
    gsmErrorCounter  =  0;
  }
  // The way that SMS display // may have to change

  if (sendATcmd("AT +CPBS=\"SM\"", "OK", 500) != 1) {
    sdErrorLog(52);
  } else {
    gsmErrorCounter  =  0;
  }

  if (sendATcmd("AT+IFC=0,0", "OK", 500) != 1) {

  } else {

  }

}

void sim900Rx(void) {
  if ((millis() - ATtimer[0]) >= ATtimer[2]) {
    if (Serial3.available()) {
      char buffer[50];
      String mad;
      char checkln = 0;

      while (Serial3.available()) {
        char xxx = (char)Serial3.read();
        if (GENARAL_RES.length() < 256) {
          GENARAL_RES += xxx;
        } else {
          GENARAL_RES = "";
        }

        if (xxx == '\r') checkln = 13;
        if (xxx == '\n') checkln = 10;
      }
      if (GENARAL_RES.indexOf("ERROR") >= 0) {
        checkln = 1;
      } else if (GENARAL_RES.indexOf("+++") >= 0)  {
        checkln = 1;
      }
      if (checkln) {
        for (int xx = 0; xx < 100; xx++) {
          if (AT_STATE[xx] >= 0) {
            strcpy_P(buffer, (char*)pgm_read_word(&(state_table[AT_STATE[xx]])));
            mad = (String)buffer;
            //          Serial.println(mad);
            if (GENARAL_RES.indexOf(mad) >= 0) {
              if (responceFlag) {
                //                  if (GENARAL_RES.indexOf("+CMGL:") >= 0) {
                //                    if (!SMSposition) {
                //                      SMSposition = GENARAL_RES.substring(GENARAL_RES.indexOf("+CMGL:") + 6, GENARAL_RES.indexOf(",")).toInt();
                //                      revSMSFlag = true;
                //                      AT_STATE[50] = -1;
                //                    }
                //                  }
                if (GENARAL_RES.indexOf("CMGR") >= 0) {
                  if (GENARAL_RES.indexOf("+CMGR:") >= 0) {
                    phoneNumber = GENARAL_RES.substring(GENARAL_RES.indexOf(",") + 2, GENARAL_RES.indexOf(",", GENARAL_RES.indexOf(",") + 2) - 1);
                    Serial.println(phoneNumber);
                    smsStore = GENARAL_RES.substring(GENARAL_RES.indexOf('\n', GENARAL_RES.indexOf("+CMGR:") + 1) + 1, GENARAL_RES.indexOf('\n', GENARAL_RES.indexOf('\n', GENARAL_RES.indexOf("+CMGR:") + 1) + 1));
                    Serial.println(smsStore);
                    SMSposition = SMS_STORE_COUNTER;
                  }
                } else {
                  AT_RESPONCE_FLAG[2] = AT_RESPONCE_FLAG[1];
                  AT_RESPONCE_FLAG[1] = AT_RESPONCE_FLAG[0];
                  AT_RESPONCE_FLAG[0] = xx;
                  AT_RESPONCE[2] = AT_RESPONCE[1];
                  AT_RESPONCE[1] = AT_RESPONCE[0];
                  AT_RESPONCE[0] = GENARAL_RES.substring(GENARAL_RES.indexOf(mad), GENARAL_RES.indexOf('\n', GENARAL_RES.indexOf(mad) + 1));
                  
//                  Serial.println(GENARAL_RES.substring(GENARAL_RES.lastIndexOf('\n', GENARAL_RES.indexOf(mad)) + 1, GENARAL_RES.indexOf('\r', GENARAL_RES.indexOf(mad))));
//                  Serial.println(mad);
//                  Serial.println(AT_RESPONCE[2]);
                }
                responceFlag = false;
                AT_STATE[xx] = -3;
                Serial.print("State No:");
                Serial.println((uint8_t)xx);
                AT_STATE_ATTEMPT[xx] = 0;
                for (int yy = 0; yy < 100; yy++)if (AT_STATE[yy] >= 0)AT_STATE[yy] = -2;
                ATFlag = false;
                GENARAL_RES = "";
                gsmErrorCounter = 0;
              } else if (mad.equals(GENARAL_RES.substring(GENARAL_RES.lastIndexOf('\n', GENARAL_RES.indexOf(mad)) + 1, GENARAL_RES.indexOf('\r', GENARAL_RES.indexOf(mad))))) {
                //                Serial.println(GENARAL_RES.substring(GENARAL_RES.lastIndexOf('\n',GENARAL_RES.indexOf(mad))+1,GENARAL_RES.indexOf('\r',GENARAL_RES.indexOf(mad))));
                //                Serial.println(mad);
                //                Serial.println("Got Respond true");
                //                Serial.println(mad);
                //                Serial.print(xx);
                //                Serial.print('\t');
                //                Serial.println(AT_STATE[xx]);
                AT_STATE[xx] = -3;
                Serial.print("State No:");
                Serial.println((uint8_t)xx);
                AT_STATE_ATTEMPT[xx] = 0;
                //                Serial.print(xx);
                //                Serial.print('\t');
                //                Serial.println(AT_STATE[xx]);
                for (int yy = 0; yy < 100; yy++)if (AT_STATE[yy] >= 0)AT_STATE[yy] = -2;
                ATFlag = false;
                GENARAL_RES = "";
                gsmErrorCounter = 0;
              }
            } else if (GENARAL_RES.indexOf("ERROR") >= 0) {
              st = 0;
              ans = 0;
#ifdef DEBUG_MPA
              Serial.print("Errorrrrr tryyyy");
              Serial.print(xx);
#endif
              AT_STATE[xx] = -4;
              AT_STATE_ATTEMPT[xx]--;
              for (int yy = 0; yy < 100; yy++) {
                if (AT_STATE[yy] >= 0) {
                  st = yy;
                  ans = AT_STATE[yy];
                  AT_STATE[yy] = -2;
                }
              }
              if (ans) {
                errorLog(st, ans);
              }
              if (AT_STATE_ATTEMPT[xx] <= 0) {
                AT_STATE[xx] = -4;
#ifdef DEBUG_MPA
                Serial.print("Zero Attempt");
                Serial.println(xx);
#endif
              }
              ATFlag = false;
              responceFlag = false;
              GENARAL_RES = "";
            }
          }
        }
      }
      mad = "";
    }
  }
  if ((millis() - ATtimer[0]) >= ATtimer[1]) {
    st = 0;
    ans = 0;
    ATFlag = false;
    responceFlag = false;
    readSMSFlag = false;
    GENARAL_RES = "";

    for (int yy = 0; yy < 100; yy++) {
      if (AT_STATE[yy] >= 0) {
        st = yy;
        ans = AT_STATE[yy];
        AT_STATE[yy] = -2;
        AT_STATE_ATTEMPT[yy]--;
#ifdef DEBUG_MPA
        Serial.print("Fail Attempt");
        Serial.println(AT_STATE_ATTEMPT[yy]);
#endif
        if (AT_STATE_ATTEMPT[yy] <= 0) {
          AT_STATE[yy] = -4;
#ifdef DEBUG_MPA
          Serial.print("Zero Attempt");
          Serial.println(yy);
#endif
        }
#ifdef DEBUG_MPA
        Serial.print("Time fail");
        Serial.println(yy);
#endif
      }
    }
    if (ans) {
      errorLog(st, ans);
    }
  }
}



uint8_t errorLog(int8_t state, int8_t answer) {
  char errorBuffer[100]  =  {0};
  String errorLogData = myTime;
  errorLogData += " ";
  errorLogData += (String)(state + 49);
  errorLogData += " ";
  errorLogData += (String)sendingDataSet.DEVICE_SN;
  strcpy_P(errorBuffer, (char*)pgm_read_word(&(errormsg_table[state + 48])));
  errorLogData += (String)errorBuffer;
  errorLogData += " ";
  strcpy_P(errorBuffer, (char*)pgm_read_word(&(state_table[answer])));
  errorLogData += (String)errorBuffer;
  errorLogData += " ";
  errorLogData += (String)gsmErrorCounter;
  Serial.println(errorLogData);
  printError(state + 49);
  gsmErrorCounter++;
}

void printError(byte errorByte) {
  byte ERROR_BUFFER[512];
  if (SDCARDclass_readblock(ERROR_COUNTER_SATRT + (errorCounter / 64), ERROR_BUFFER) == 0) {
    union {
      unsigned long a;
      byte b[4];
    } bconvf;
    bconvf.a = errorCounter; // write error counter
    for (int i = 0; i < 3; i++) {
      ERROR_BUFFER[(errorCounter % 64) * 8 + i] = bconvf.b[i];
    }
    bconvf.a = sendingDataSet.TIME_STAMP; // write Time Stamp
    for (int i = 0; i < 4; i++) {
      ERROR_BUFFER[(errorCounter % 64) * 8L + 3L + i] = bconvf.b[i];
    }
    ERROR_BUFFER[(errorCounter % 64) * 8 + 7] = errorByte;// write error byte
    if (SDCARDclass_writeblock(ERROR_COUNTER_SATRT + (errorCounter / 64), ERROR_BUFFER) == 0) {
      errorCounter++;
    }
  }
}


void responceReader(byte state, char * resp) {
  for (int ii = 0; ii < 3; ii++) {
    if (state == AT_RESPONCE_FLAG[ii]) {
      AT_RESPONCE[ii].toCharArray(resp, 100);
    }
  }
}

char ATCondition(byte state) {
  char ret = 0;
  if ((AT_STATE[state] < 0) && (!ATFlag) && (AT_STATE[state] > -3) && (!CONNECTION_OPEN)) {
    ret = 1;
  }
  return ret;
}

char ATCondition(byte state, byte state2) {
  char ret = 0;
  if (!CONNECTION_OPEN) {
    for (int y = state ; y < state2 + 1; y++) {
      if ((AT_STATE[y] < 0) && (!ATFlag) && (AT_STATE[y] > -3)) {
        ret = 1;

      }
    }
    for (int y = state ; y < state2 + 1; y++) {
      if (AT_STATE[y] == -3) {
        ret = 0;
      }
    }
  }
  return ret;
}

char resCondition(byte state) {
  char ret = 0;
  if ((AT_STATE[state] == -3) && (!responceFlag)) {
    ret = 1;
  }
  return ret;
}
char AT_STATE_READER(byte state) {
  char ret = 0;
  if (AT_STATE[state] == -3) {
    ret = 1;
  }
  return ret;
}

char sendAT(char* ATcommand, char expecState, char state, unsigned int timeout, unsigned int readStartTime, boolean res, byte attempt) {

  while ( Serial3.available() > 0) Serial3.read();
  Serial3.print(ATcommand);
  Serial3.flush();
  if (AT_STATE_ATTEMPT[state] <= 0) {
    AT_STATE_ATTEMPT[state] = attempt;
#ifdef DEBUG_MPA
    Serial.print("AT Attempt");
    Serial.println(AT_STATE_ATTEMPT[state]);
#endif
  }
  //  Serial.print("State No:");
  //  Serial.println((uint8_t)state);
  ATtimer[1] = timeout;
  ATtimer[0] = millis();
  ATtimer[2] = readStartTime;
  ATFlag = true;
  AT_STATE[state] = expecState;
  responceFlag = res;
}

char sendATx( char expecState, char state, unsigned int timeout, unsigned int readStartTime, boolean res) {

  //  while ( Serial3.available() > 0) Serial3.read();
  //  Serial3.println(ATcommand);
  ATtimer[1] = timeout;
  ATtimer[0] = millis();
  ATtimer[2] = readStartTime;
  ATFlag = true;
  AT_STATE[state] = expecState;
  responceFlag = res;
}

char sendAT2(char* ATcommand, char expecState, char state, char unExpecState, unsigned int timeout, unsigned int readStartTime, boolean res) {
  if (!TCP_CONNECT_FLAG) {
    while ( Serial3.available() > 0) Serial3.read();
    Serial3.println(ATcommand);
    Serial3.flush();
    ATtimer[1] = timeout;
    ATtimer[0] = millis();
    ATtimer[2] = readStartTime;
    ATFlag = true;
    AT_STATE[state] = expecState;
    responceFlag = res;
  }
}

char sendRangeAT(char* ATcommand, char expecStateStart, char stateStart, char rangeSize, unsigned int timeout, unsigned int readStartTime, boolean res , byte attempt) {

  while ( Serial3.available() > 0) Serial3.read();
  Serial3.println(ATcommand);
  Serial3.flush();
  //  Serial.println(ATcommand);
  ATtimer[1] = timeout;
  ATtimer[0] = millis();
  ATtimer[2] = readStartTime;
  ATFlag = true;
  Serial.print("State No:");
  Serial.println((uint8_t)stateStart);
  for (int jj = 0; jj < rangeSize; jj++) {
    AT_STATE[stateStart + jj] = expecStateStart + jj;

    if (AT_STATE_ATTEMPT[stateStart + jj] <= 0) {
      AT_STATE_ATTEMPT[stateStart + jj] = attempt;
#ifdef DEBUG_MPA
      Serial.print(stateStart + jj);
      Serial.print("AT Attempt");
      Serial.println(AT_STATE_ATTEMPT[stateStart + jj]);
#endif
    }
  }
  responceFlag = res;
}


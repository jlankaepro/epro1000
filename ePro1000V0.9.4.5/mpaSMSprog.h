
/*
 SIM900.print("AT+CMGF=1\r");                                                        // AT command to send SMS message
  delay(100);
  SIM900.println("AT + CMGS = \"+12128675309\"");                                     // recipient's mobile number, in international format
  delay(100);
  SIM900.println("Hello, world. This is a text message from an Arduino Uno.");

  */
void sendSMS(String message, String pn)
{
  char buff[100];
  char pnbuff[15];

  if (sendATcmd("AT+CMGF=1\r", "OK", 500)) { // AT command to send SMS message
    pn.toCharArray(pnbuff, pn.length() + 1);
    sprintf(buff, "AT + CMGS = \"%s\"\r" , pnbuff); // "OK"
    if (sendATcmd(buff, ">", 500)) {
      Serial3.print(message); // message to send
      char a[1] = {0x1a};
      sendATcmd(a, "+CMGS:", 5000);
    } else {
      char a[1] = {0x1a};
      sendATcmd(a, "OK", 15000);
    }
  }
  Serial3.println();
  delay(6500);
}





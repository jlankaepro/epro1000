


char gprsSendSetup() {
  char buff[100];
  if ((AT_STATE[25] <= -2) || ((AT_STATE[28] <= -2))) {
    for (int x = 6; x < 50; x++)AT_STATE[x] = -1;
  }
  if (ATCondition(3)) {
    Serial.print("State No:");
    Serial.println((uint8_t)78);
    sendAT("AT\r\n", 0, 3, 500, 100, 0, 2);
    //    AT_STATE[1] = -1;
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(4)) {
      sendAT("AT+CREG=1\r\n", 0, 4, 500, 100, 0, 2);
    }
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(5)) {
      sendAT("AT +CREG?\r\n", 13, 5, 500, 100, 0, 2);
    }
  }
  if (AT_STATE_READER(3)) {
    if (ATCondition(6, 15)) {
      sendRangeAT("AT +CIPSTATUS", 1, 6, 10, 500, 100, 0, 2); //"IP INITIAL"
      Serial.println("AT +CIPSTATUS");
    }
  }

  if (AT_STATE_READER(7)) {
    if (ATCondition(23)) {
      sendAT("AT +CIPCLOSE = 1\r\n", 11, 23, 500, 100, 0, 3); // "CLOSE OK"
    }
  }
  if (AT_STATE_READER(13)) {
    if (ATCondition(28)) {
      sendAT("AT+CGATT=0\r\n", 0, 28, 700, 100, 0, 3); // "OK"
    }
  }
  if ((AT_STATE_READER(11)) || (AT_STATE_READER(14))) {
    if (ATCondition(25)) {
      sendAT("AT +CIPSHUT\r\n", 21, 25, 500, 100, 0, 3); // "CLOSE OK"
    }
  }

  if (AT_STATE_READER(6)) {
    if (ATCondition(15)) {
      sendAT("AT +CIPMUX=0\r\n", 0, 15, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(16)) {
      sendAT("AT+CIPMODE=1\r\n", 0, 16, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(24)) {
      sendAT("AT+CGATT =1\r\n", 0, 24, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(17)) {
      sendAT("AT+CGATT?\r\n", 0, 17, 500, 100, 0, 3); // "OK"
    }
  }
  if (AT_STATE_READER(6)) {
    if (ATCondition(18)) {
      sprintf(buff, "AT +CSTT=\"%s\"\r\n", APN);// "OK"
      sendAT(buff, 0, 18, 500, 100, 0, 3);
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(10))) {
    if (ATCondition(19)) {
      sendAT("AT +CIICR\r\n", 0, 19, 20000, 200, 0, 2); // "OK"
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(9)) || (AT_STATE_READER(10))) {
    if (ATCondition(20)) {
      sendAT("AT +CIFSR\r\n", 16, 20, 500, 100, 1, 3);
    }
  }
  if ((AT_STATE_READER(6)) || (AT_STATE_READER(7)) || (AT_STATE_READER(12)) || (AT_STATE_READER(8)) || (AT_STATE_READER(9)) || (AT_STATE_READER(10))) {
    //if (((AT_STATE_READER(6)) && (AT_STATE_READER(18)) && (AT_STATE_READER(19)) && (AT_STATE_READER(20))) || ((AT_STATE_READER(23))&&(AT_STATE_READER(7))) || (AT_STATE_READER(8)) || ((AT_STATE_READER(20)) && ((AT_STATE_READER(9)) || (AT_STATE_READER(10))))) {
    if (ATCondition(21)) {
      sprintf(buff, "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n", IP, port);
      sendAT(buff, 20, 21, 30000, 100, 0, 2);
    }
  } else {
    //    restartFlagDisable();
  }
}

void sendRemainData() {

  String SrvResp = "";
  char temp[100];
  uint32_t NO_OF_PACKET = 0;
  uint32_t POSISION_OF_SD = 0;
  byte sendRemainDataChar[512];

  responceReader(26, temp);
  SrvResp = (String)temp;

  if (SrvResp.lastIndexOf("SERVER:") >= 0) {
    Serial.println(SrvResp);
    POSISION_OF_SD = SrvResp.substring(7, SrvResp.indexOf(',')).toInt();
    Serial.println(POSISION_OF_SD);
    NO_OF_PACKET = SrvResp.substring(SrvResp.indexOf(',') + 1).toInt();
    Serial.println(NO_OF_PACKET);
  }
  //  if(((NO_OF_PACKET > 5) || ((NO_OF_PACKET < 5))) && (NO_OF_PACKET != 0) )NO_OF_PACKET = 5;
  if ((NO_OF_PACKET > 30)) NO_OF_PACKET = 30;
  Serial.print("State No:");
  Serial.println((uint8_t)77);
  for (byte z = 0; z < NO_OF_PACKET; z++) {
    if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[0] + ((uint32_t)POSISION_OF_SD + z) / 4), sendRemainDataChar) == 0) {
      Serial3.flush();
      for (int con = (((POSISION_OF_SD + z) % 4) * 128); con < (((POSISION_OF_SD + z) % 4) * 128) + 128; con++) {
        Serial3.write(sendRemainDataChar[con]);
        Serial.print(sendRemainDataChar[con]);
        Serial.print(' ');
      }
      Serial.println();

    }

  }
  delay(2000);
}


//if (SDCARDclass_readblock(uint32_t(DEVICE_SD_START[deviceState] + ((DEVICE_SD_COUNTERS[deviceState] - y) / 4)), SD_DATA_BUFFER) == 0) {
//        for (uint16_t con = (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128); con < (((DEVICE_SD_COUNTERS[deviceState] - y) % 4) * 128) + 128; con++) {
//          Serial3.write(SD_DATA_BUFFER[con]);
//          Serial.print(SD_DATA_BUFFER[con]);
//          Serial.print(' ');

void TCP_CLOSE() {
  digitalWrite(DTR, HIGH);
  digitalWrite(DTR, LOW);
  delay(1500);
  digitalWrite(DTR, HIGH);
  if ((AT_STATE[27] < 0) && (!ATFlag) && (AT_STATE[27] > -3)) {
    sendAT("+++", 17, 27, 1000, 200, 0, 4);
    Serial.print("+++");
  }
}

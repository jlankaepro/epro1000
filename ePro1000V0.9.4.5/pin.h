// connected pins wrf to Arduino
#define CS7758 28

// used for Maduks sub
#define micGate 0
#define sdChipSelect 27
#define SIM900_POWER 49
#define rs485RE 32
#define rs485DE 31
#define RTS 54
#define CTS 55
#define DTR 38
#define DCS 40

//###################################
// old pin defs
// pins definition
#define GSM_ON              26
// connect GSM Module turn ON
#define GSM_STATUS        62

const int scePin = 24;   // SCE - Chip select, pin 3 on LCD.
const int rstPin = 23;   // RST - Reset, pin 4 on LCD.
const int dcPin = 25;    // DC - Data/Command, pin 5 on LCD.
const int sdinPin = 51;  // DN(MOSI) - Serial data, pin 6 on LCD.
const int sclkPin = 52;  // SCLK - Serial clock, pin 7 on LCD.
const int blPin = 45;    // LED - Backlight LED, pin 8 on LCD.

void pinSetup();

void pinSetup()
{

  pinMode(rs485RE,OUTPUT);
  pinMode(rs485DE,OUTPUT);
  pinMode(SIM900_POWER,OUTPUT);
  pinMode(DCS,INPUT);
  pinMode(RTS,INPUT);
  pinMode(DTR,OUTPUT);
  pinMode(CTS,OUTPUT);
  pinMode(CS7758, OUTPUT);
  pinMode(sdChipSelect, OUTPUT);
  pinMode(scePin, OUTPUT); //LCD CS pin
  pinMode(blPin, OUTPUT);// defined as 2 places
  pinMode(GSM_ON, OUTPUT);
  pinMode(GSM_STATUS, INPUT);
  pinMode(43,INPUT);
  pinMode(52, OUTPUT);//SCK
  pinMode(50, INPUT);//MISO
  pinMode(51, OUTPUT);//MOSI

  digitalWrite(SIM900_POWER, HIGH);
  digitalWrite(CS7758, HIGH);
  digitalWrite(sdChipSelect, HIGH);
  digitalWrite(scePin, HIGH); //LCD CS pin
  digitalWrite(blPin, LOW); //LCD BL pin
  digitalWrite(GSM_ON, LOW);

}
